local skynet = require "skynet"


skynet.start(function()
	-- skynet.uniqueservice("reloadd")
	skynet.uniqueservice("confcenter")
	--skynet.uniqueservice("broadcasting_service")	
	
	local manager = skynet.uniqueservice("manager_service")
	skynet.call(manager, "lua", "start")

	skynet.exit()
end)

