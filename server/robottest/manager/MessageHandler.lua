--
-- @Author:      name
-- @DateTime:    2018-03-30 23:05:48
-- @Description: 消息的处理

local skynet = require "skynet"
local log = require "Logger"
local config = require "configquery"
local NodeMessage = require "NodeMessage"
-- local MessagePack = require "MessagePack"
local ProtoLoader = require "ProtoLoader"
-- local MessageSession = require "MessageSession"

-- local AssertEx = AssertEx
-- require "common"



local MessageHandler = class("MessageHandler")

---------------------------------------------------------
-- Private
---------------------------------------------------------
function MessageHandler:ctor(message_dispatch)

	self.server_id = skynet.getenv("server_id") --节点名
	self.proto_file = skynet.getenv("proto_config") --proto文件路径	
	self.message_dispatch = message_dispatch	
	
		
	self.node_message = NodeMessage.new()		
	self.proto_loader = ProtoLoader.new() --proto加载
	self:register()
	--清除socket
	-- skynet.fork(function()
	-- 	self:loop()
	-- end)	
end

--注册本服务里的消息
function MessageHandler:register()

	self.message_dispatch:registerSelf('start',handler(self,self.start))

end


---------------------------------------------------------
-- CMD
---------------------------------------------------------
function MessageHandler:start()

	-- print("___proto_file_",self.proto_file)
    local pbc_env = self.proto_loader:init(self.proto_file, {'common','game', 'gate','web','zone','hall',})
	-- local svr_config = config.setting_cfg["robotserver"][self.server_id]
	
	--skynet控制台
	-- skynet.newservice("debug_console",svr_config.debug_port)

	-- --数据库
 -- 	self.node_message:callProxy("dbserver","load_tables",{{name="users", pk='id',key='name' },})	

 	local user_list = {
 		{
 			account = "test9",
 			password = "123456",
 		},
 		{
 			account = "test10",
 			password = "123456",
 		}, 		
 	}
	--socket消息处理
	local service
	for i = 1, 2 do
		service = skynet.newservice("robot_service")		
		skynet.call(service, 'lua', 'init', pbc_env, user_list[i])
	end

	
end





return MessageHandler