--
-- Author:      name
-- DateTime:    2018-04-23 17:20:15
-- Description: 处理socket消息的服务

local skynet = require "skynet"
local log = require "Logger"


local MessageDispatch = require "MessageDispatch"
local MessageHandler = require "robot.MessageHandler"
local NodeMessage = require "NodeMessage"


local message_dispatch 
local message_handler
node_message = NodeMessage.new()
---------------------------------------------------------
-- Local
---------------------------------------------------------



--此服务用到的对象
local function init()
	--消息派发
	message_dispatch = MessageDispatch.new()
	--处理服务间消息
	message_handler = MessageHandler.new(message_dispatch)

	--登出处理
	

	-- --可热更的对象
	-- g_objects:add(userObj)
	-- g_objects:add(command)	
	-- g_objects:add(otherNode)	
	-- g_objects:add(loginObj)	
	-- g_objects:add(logoutObj)
	-- g_objects:add(tableObj)
	-- g_objects:hotfix("gate")	

end


-----------------------------------------------------------------------------------------
--skyent
-----------------------------------------------------------------------------------------

skynet.start(function()

	init()
	--socket消息
	skynet.register_protocol {
		name = "client",
		id = skynet.PTYPE_CLIENT,
		--消息解包
		unpack = handler(message_handler, message_handler.messageUnpack),
		--从客户发来的消息
		dispatch = handler(message_handler, message_handler.clientMessage),
	}
	--处理服务消息
	skynet.dispatch("lua", message_dispatch:dispatch())
end)