--
-- @Author:      name
-- @DateTime:    2018-08-9 23:05:48
-- @Description: 消息处理

local skynet = require "skynet"
local queue = require "skynet.queue"
local log = require "Logger"
local Network = require "Network"
-- require "MessageCMD"
local config = require "configquery"



local MessageHandler = class("MessageHandler")

---------------------------------------------------------
-- Private
---------------------------------------------------------
function MessageHandler:ctor(message_dispatch)
	
	self.message_dispatch = message_dispatch	
	self.user_sockets = {} --用户连接的基本信息
	self.node_message = node_message
	local ip = "127.0.0.1"
	-- local ip = "192.168.2.21"
	local port = 12101
	self.network = Network.new(ip,port)

	self.user_id = nil
	self.banker_id = nil

	self.game_user = nil

	self:register()

	--登录
	skynet.fork(function()
		skynet.sleep(200)
		self:userLoginReq()
		-- self:userRegister()
		-- self:loginAuthReq()
	end)
end

--注册本服务里的消息
function MessageHandler:register()

	self.message_dispatch:registerSelf('init',handler(self,self.init))

	local package_name = "proto.msg.login."
	-- self:registerByName("MSG_L2C_LoginSuccess")
	self:registerByName("userLoginRes")
	self:registerByName("userRegisterRes")

	local package_name = "proto.msg.gate."
	self:registerByName("userLoginAuthRes")
	self:registerByName("kickUserNt")
	self:registerByName("userLogoutRes")
	
	
	package_name = "proto.msg.hall."
	self:registerByName("MSG_H2C_GameCategoryList")
	self:registerByName("MSG_H2C_GameInfoList")
	self:registerByName("enterHallRes")
	self:registerByName("enterGameRes")
	self:registerByName("MSG_H2C_GameClassifyList")


	package_name = "proto.msg.game."
	self:registerByName("enterDeskRes")
	self:registerByName("leaveDeskRes")
	self:registerByName("deskStatusRes")
	self:registerByName("sitDownRes")
	self:registerByName("standUpRes")
	-- self:registerByName("MSG_G2C_ChangeDeskResult")
	-- self:registerByName("MSG_G2C_UserReadyResult")
	-- self:registerByName("MSG_G2C_GameConfig")
	-- self:registerByName("MSG_G2C_UserInfo")
	-- self:registerByName("MSG_G2C_EnterFinish")
	-- self:registerByName("MSG_G2C_UserStatusInfo")
	-- self:registerByName("MSG_G2C_ClientReadyResult")	
	-- self:registerByName("MSG_G2C_GameStartNotify")	

	self:registerByName("nnStartNt")
	self:registerByName("nnBankNt")
	self:registerByName("nnBetNt")
	self:registerByName("nnLeftCardNt")
	self:registerByName("nnEndNt")
	self:registerByName("nnRestNt")
	self:registerByName("nnWaitNt")
	self:registerByName("nnUserBankNt")
	self:registerByName("nnBankOkNt")

	self:registerByName("nnSeatChangeNt")
	self:registerByName("nnUserBetNt")	
	self:registerByName("nnUserOpenCardNt")			
	
	

	self:registerByName("nnBetRes")
	self:registerByName("nnBankRes")
	self:registerByName("nnOpenCardRes")

end

function MessageHandler:init(pbc_env, user_info)
	print("___________init__________",pbc_env)
	self.cmd = config.message_cmd
	self.user_info = user_info
	self.network:init(pbc_env)
end

function MessageHandler:registerByName( message_name)
	self.network:register(message_name, handler(self,self[message_name]))
end

function MessageHandler:print(...)
	if self.user_id == 106 then 
		print(...)
	end
end
-------------req
--用户注册
function MessageHandler:userRegister()
	local data = {
		domain_id = 1,
		device_type = 'android',
		channel_id = 1,
		account = "test102",--..math.random(1,100),
		password = "123456",
		nickname = "test",			
		face_id = 0,
		sex = 1,			
	}
	self.network:send("userRegisterReq",data)
end
--用户登陆
function MessageHandler:userLoginReq()
-- //用户登陆
-- message MSG_UserLogin
-- {
-- 	required string		version			= 1;	// 版本号
-- 	required string		mac_addr		= 2;	// MAC物理地址(UUID)
-- 	optional uint32		device_type		= 3;	// 手机设备
-- 	required uint32		channel_id		= 4;	// 渠道标识
-- 	required uint32		login_type  	= 5;	// 登录类型
-- 	optional string		accounts 		= 6;	// 登录帐号
-- 	optional string		password 		= 7;	// 登录密码
-- 	optional uint32		user_id			= 8;	// ID登录方式
-- 	optional string		token			= 9;	// 登录token
-- }

	local data = {
		version = "1.0",
		mac_addr = "123",
		device_type = 1,
		channel_id = 1,
		login_type = 2,
		account = self.user_info.account,
		password = self.user_info.password,
		user_id = 1,
		token = '',
		mac_addr = "12324314",
	}
	self.network:send("userLoginReq",data)
end
--认证登陆
function MessageHandler:MSG_C2L_AuthLogin(data)

end
--登陆认证
function MessageHandler:loginAuthReq(data)
	-- local body = {
	-- 	user_id = 39,
	-- 	token = 'MTU1NjAwNzE0OQ==',
	-- }
	-- self:print("_____data__",data)
	self.network:send("userLoginAuthReq",data)
end

--请求进入大厅
function MessageHandler:enterHallReq()
	local data = {
		user_id = 11,
	}
	self.network:send("enterHallReq",data)
end

--取桌子id
function MessageHandler:enterGameReq()
	local body = {
		game_id = 1,
		group_id = 1,
	}
	self.network:send("enterGameReq",body)	
end








-------------res
--登录成功
function MessageHandler:userLoginAuthRes(data)
	--token认证
	self:print("___token认证————",data)
	if data.error_code == 0 then 
		self:enterHallReq()
	end
end

function MessageHandler:userLoginRes(data)

	print("___登录服务器登录结果____",data)
	if not data.error_code or data.error_code == 0 then 
		print("___登录服器成功通过___")
		--切换登录网关
		--用token去验证登录
		self.network:close()
		self.user_id =  data.user_info.user_id
		local addr = data.server_addr
		local ip,port = string.match(addr,"^([^:]*):(.*)$")
		skynet.fork(function()
			skynet.sleep(100)	
			self.network:open(ip,port)
			local data = {
				user_id = data.user_info.user_id,
				token = data.token,
			}		
			self:loginAuthReq(data)
		end)
	end	
end
--注册结果
function MessageHandler:userRegisterRes(data)
	if data.error_code and data.error_code > 0 then 
		self:print("______",data.error_msg)
		return
	end
	self:print("___注册成功____",data)
end

function MessageHandler:kickUserNt(data)
	print("_kickUserNt__",data)
	os.exit()
end

function MessageHandler:enterHallRes(data)
	self:print("____进入大厅返回",data)

	self:enterGameReq()	
end

function MessageHandler:MSG_H2C_GameCategoryList(data)

	self:print("___大厅推送游戏种类成功____",data)
end

--游戏列表
function MessageHandler:MSG_H2C_GameInfoList(data)
-- message MSG_GameCategory
-- {
--     required uint32 category_id     = 1;        //种类标识
--     required string category_name   = 2;        //种类名称
-- }

-- //游戏种类列表
-- message MSG_H2C_GameCategoryList
-- {
--     repeated MSG_GameCategory game_category_list = 1;   //游戏种类列表        
-- }
	self:print("___大厅推送游戏列表成功____")

	-- --登出
	-- local data = {
	-- 	user_id = self.user_id,
	-- }
	-- self.network:send(self.cmd.DMSG_MDM_LOGIN, self.cmd.DMSG_C2L_UserLogout,data)

	self:enterGameReq()	
end

--游戏列表
function MessageHandler:MSG_H2C_GameClassifyList(data)
	self:print("___大厅推送游戏列表成功_11___")

end

--登出
function MessageHandler:userLogoutRes(data)

	self:print("____登出返回____________",data)
end

function MessageHandler:enterGameRes(data)
	self:print("____取游戏桌子id返回____________",data)
	if data.desk_id and data.desk_id>0 then 
		self:enterDeskReq(data.desk_id)
	end

end

--------------游戏协议----------------------------------------------

------------------------req
--进入桌子
function MessageHandler:enterDeskReq(desk_id)
	local body = {
		desk_id = desk_id,
	}
	self.network:send("enterDeskReq",body)
end
function MessageHandler:enterDeskRes(data)
	self:print("_进入桌子__enterDeskRes_____",data)
	if data.error_code==0 then
		self.desk_id = data.desk_id
		self:deskStatusReq()
	end
end

function MessageHandler:leaveDeskReq(data)
	local body = {
		desk_id = 1,
	}
	self.network:send(self.cmd.DMSG_MDM_GAME,self.cmd.DMSG_C2G_LeaveDesk,body)
end
function MessageHandler:leaveDeskRes(data)
	self:print("_离开桌子_MSG_G2C_LeaveDeskResult______",data)
end

function MessageHandler:sitdownReq(data)
	local body = {
		desk_id = 1,
		seat_id = -1,
		desk_password = nil,
	}
	self.network:send("sitdownReq",body)
end
function MessageHandler:standupReq(data)
	local body = {
		desk_id = 1,
		seat_id = 1,
		force_leave	= nil,	
	}
	self.network:send("standupReq",body)
end

--请求桌子状态
function MessageHandler:deskStatusReq()
	local body = {
		desk_id = self.desk_id,			
	}
	self.network:send("deskStatusReq",body)	
end
function MessageHandler:deskStatusRes(data)
	print("____deskStatusRes__",data)
end


function MessageHandler:MSG_C2G_ChangeDesk(data)
	local body = {
		desk_id = 1,
		seat_id = 1,
	}
	self.network:send(self.cmd.DMSG_MDM_GAME,self.cmd.DMSG_C2G_ChangeDesk,body)
end

--客户端准备
function MessageHandler:MSG_C2G_ClientReady(data)
	local body = {
		allow_lookon = 1,
	}
	self.network:send(self.cmd.DMSG_MDM_GAME,self.cmd.DMSG_C2G_ClientReady,body)
end

--用户准备
function MessageHandler:MSG_C2G_UserReady(data)
	local body = {
		extend_data = {1},
	}
	self.network:send(self.cmd.DMSG_MDM_GAME,self.cmd.DMSG_C2G_UserReady,body)
end


------------------------res

function MessageHandler:sitDownRes(data)
	self:print("__坐下__sitdownReqResult____",data)
end
function MessageHandler:standUpRes(data)
	self:print("__站起_standUpRes_____",data)
end

function MessageHandler:deskUserReq(data)
	print("______",data)
end
function MessageHandler:deskUserRes(data)
	print("___deskUserRes___",data)
end




-------------------niuniu
function MessageHandler:nnStartNt(data)
	print("___nnStartNt___",data)
end
function MessageHandler:nnBankNt(data)
	print("____nnBankNt__",data)
	self:nnBankReq()
end
function MessageHandler:nnBetNt(data)
	print("__nnBetNt____",data)
	if self.banker_id==self.user_id then 
		return 
	end
	self:nnBetReq()
end
function MessageHandler:nnLeftCardNt(data)
	print("___nnLeftCardNt___",data)	
	self:nnOpenCardReq()
end
function MessageHandler:nnEndNt(data)
	print("__nnEndNt____",data)
end
function MessageHandler:nnRestNt(data)
	print("___nnRestNt___",data)
end
function MessageHandler:nnWaitNt(data)
	print("__nnWaitNt____",data)
end
--玩家抢庄通知
function MessageHandler:nnUserBankNt(data)
	print("___nnUserBankNt___",data)
end
function MessageHandler:nnBankOkNt(data)
	print("___nnBankOKNt___",data)
	self.banker_id = data.user_id
end

function MessageHandler:nnSeatChangeNt(data)
	print("______",data)
end
function MessageHandler:nnUserBetNt(data)
	print("___nnUserBetNt___",data)
end
function MessageHandler:nnUserOpenCardNt(data)
	print("__nnUserOpenCardNt____",data)
end
------------------------------

------------------------------

function MessageHandler:nnBetReq(data)	
	local data = {
		desk_id=self.desk_id,
		rate=1,
	}
	self.network:send("nnBetReq", data)	
end
function MessageHandler:nnBetRes(data)
	print("___nnBetRes___",data)
end

function MessageHandler:nnBankReq(data)
	local data = {
		desk_id=self.desk_id,
		rate=1,
	}
	self.network:send("nnBankReq", data)
end
function MessageHandler:nnBankRes(data)
	print("__nnBankRes____",data)
end

function MessageHandler:nnOpenCardReq(data)
	local data = {
		desk_id=self.desk_id,
	}
	self.network:send("nnOpenCardReq", data)
end
function MessageHandler:nnOpenCardRes(data)
	print("__nnOpenCardRes____",data)
end

return MessageHandler