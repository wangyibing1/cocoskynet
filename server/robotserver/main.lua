local skynet = require "skynet"


skynet.start(function()

	skynet.uniqueservice("confcenter")
	local manager = skynet.uniqueservice("manager_service")
	skynet.call(manager, "lua", "start")

	skynet.exit()
end)

