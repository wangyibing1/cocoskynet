--
-- @Author:      
-- @DateTime:    2019-04-28 23:05:48
-- @Description: 分配服务器管理


local log = require "Logger"

local AssignManager = class("AssignManager")

--构造函数
function AssignManager:ctor()

    self.user_assign_list = {} 
end

--------------------
--添加用户分配信息
function AssignManager:addUserAssign(assign_info)
    local user_id = assign_info.user_id

    if not self.user_assign_list[user_id] then
        self.user_assign_list[user_id] = assign_info
        return true
    else
        return false
    end
end

--更新用户分配信息
function AssignManager:updateUserAssign(assign_info)
    local user_id = assign_info.user_id
    if not self.user_assign_list[user_id] then 
        return false
    end

    local function _update(des, src)
        for k, v in pairs(src) do
            if type(v) == 'table' then
                if not des[k] then des[k] = {} end
                _update(des[k], v)
            else
                des[k] = v
            end
        end
    end

    _update(self.user_assign_list[user_id], assign_info)

    return true
end

--移除用户分配信息
function AssignManager:removeUserAssign(user_id)

    if self.user_assign_list[user_id] then 
        self.user_assign_list[user_id] = nil
        return true
    end

    return false
end

--获取用户分配信息
function AssignManager:getUserAssign(user_id)
    return self.user_assign_list[user_id]
end

--获取用户分配列表
function AssignManager:getUserAssignList()
    return self.user_assign_list
end


return AssignManager
