--
-- Author:      name
-- DateTime:    2018-04-23 17:19:33
-- Description: 服务管理


require "skynet.manager"
local skynet = require "skynet"
local NodeMessage = require "NodeMessage"
local MessageDispatch = require "MessageDispatch"
local MessageHandler = require "MessageHandler"
local ClusterClient = require "ClusterClient"
local ServerManager = require "ServerManager"
local AssignManager = require "AssignManager"
local GameManager = require "GameManager"

global = {}

local function init()
	local message_dispatch = MessageDispatch.new()	
	local node_message = NodeMessage.new()
	local message_handler = MessageHandler.new(message_dispatch, node_message)
	local cluster_client = ClusterClient.new(message_dispatch, node_message)	
	local server_manager = ServerManager.new()
	local assign_manager = AssignManager.new()
	local game_manager = GameManager.new()

	global.message_dispatch = message_dispatch
	global.node_message = node_message
	global.message_handler = message_handler
	global.cluster_client = cluster_client
	global.server_manager = server_manager
	global.assign_manager = assign_manager
	global.game_manager = game_manager

	--开启集群节点
	cluster_client:openCluster()
	skynet.dispatch("lua", message_dispatch:dispatch())	
end
---------------------------------------------------------
-- skynet
---------------------------------------------------------
skynet.start(function()

	init()
	skynet.register('.proxy')

end)
