--
-- @Author:      dreamfly
-- @DateTime:    2019-04-28 23:05:48
-- @Description: 游戏分配管理


local log = require "Logger"

local GameManager = class("GameManager")

--构造函数
function GameManager:ctor()

    self.game_room_list = {} 
end

--------------------
--添加游戏房间信息
function GameManager:addGameRoom(room_info)
    local server_id = room_info.server_id

    self.game_room_list[server_id] = room_info
    self.game_room_list[server_id].desk_list = {}
end

--更新游戏房间信息
function GameManager:updateGameRoom(room_info)
    local server_id = room_info.server_id
    if not self.game_room_list[server_id] then 
        return false
    end

    local function _update(des, src)
        for k, v in pairs(src) do
            if type(v) == 'table' then
                if not des[k] then des[k] = {} end
                _update(des[k], v)
            else
                des[k] = v
            end
        end
    end

    _update(self.game_room_list[server_id], room_info)

    return true
end

--移除游戏房间信息
function GameManager:removeGameRoom(server_id)

    if self.game_room_list[server_id] then 
        self.game_room_list[server_id] = nil
        return true
    end

    return false
end

--获取游戏房间信息
function GameManager:getGameRoom(server_id)
    return self.game_room_list[server_id]
end
function GameManager:getGameRoomInfo(server_id, group_id)
    local info = self.game_room_list[server_id]
    if info and info[group_id] then
        return info[group_id]
    end
end

--获取游戏房间列表
function GameManager:getGameRoomList()
    return self.game_room_list
end

--添加游戏房间桌子信息
function GameManager:addGameRoomDesk(server_id, desk_info)

    if self.game_room_list[server_id] then
        if not self.game_room_list[server_id].desk_list then
            self.game_room_list[server_id].desk_list = {}
        end

        local desk_id = desk_info.desk_id
        self.game_room_list[server_id].desk_list[desk_id] = desk_info
        return true
    end

    return false
end

--更新游戏房间桌子信息
function GameManager:updateGameRoomDesk(server_id, desk_info)

    if not self.game_room_list[server_id] then 
        return false
    end

    local desk_id = desk_info.desk_id
    if not self.game_room_list[server_id].desk_list or not self.game_room_list[server_id].desk_list[desk_id] then
        return false
    end

    local function _update(des, src)
        for k, v in pairs(src) do
            if type(v) == 'table' then
                if not des[k] then des[k] = {} end
                _update(des[k], v)
            else
                des[k] = v
            end
        end
    end

    _update(self.game_room_list[server_id].desk_list[desk_id], desk_info)

    return true
end

--移除游戏房间桌子信息
function GameManager:removeGameRoomDesk(server_id, desk_id)

    if self.game_room_list[server_id] then 
        if self.game_room_list[server_id].desk_list and self.game_room_list[server_id].desk_list[desk_id] then
            self.game_room_list[server_id].desk_list[desk_id] = nil
            return true
        end
    end

    return false
end

--获取游戏房间桌子信息
function GameManager:getGameRoomDesk(server_id, desk_id)
    if self.game_room_list[server_id] and self.game_room_list[server_id].desk_list then
        return self.game_room_list[server_id].desk_list[desk_id]
    end
    
    return nil
end

--获取游戏房间桌子列表
function GameManager:getGameRoomDeskList(server_id)
    if self.game_room_list[server_id] then
        return self.game_room_list[server_id].desk_list
    end

    return nil
end


return GameManager
