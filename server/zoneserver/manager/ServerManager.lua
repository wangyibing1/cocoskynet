--
-- @Author:      name
-- @DateTime:    2019-04-28 23:05:48
-- @Description: 服务器管理


local log = require "Logger"

local ServerManager = class("ServerManager")

--构造函数
function ServerManager:ctor(game_manager)
    -- local server = {
    --     server_id = 0,
    --     server_type = 0,
    --     server_addr = "",
    --     server_name = "",
    --     cluster_id = "",
    --     protocol = "",
    --     server_port,
    --     server_wport,    
    -- }
    self.server_list = {} --服务器列表
    self.game_manager = game_manager
end

--添加服务器信息
function ServerManager:addServer(server)
    local server_id = server.server_id
    if self.server_list[server_id] then 
        return false
    end
    self.server_list[server_id] = server
    return true
end

--移除服务器信息
function ServerManager:removeServer(server_id)

    if self.server_list[server_id] then 
        self.server_list[server_id] = nil
        return true
    end

    return false
end

--获取服务器信息
function ServerManager:getServer(server_id)
    return self.server_list[server_id]
end

--获取服务器列表
function ServerManager:getServerList()
    return self.server_list
end

--获取服务器信息
function ServerManager:getServerListByType(server_type)    
    local server_list = {}
    for _, v in pairs(self.server_list) do         
        if v.server_type == server_type then 
            table.insert(server_list, v)
        end
    end    

    return server_list
end

--更新服务器信息
function ServerManager:updateServer(server)
    local server_id = server.server_id
    local protocol = server.protocol

    if not self.server_list[server_id] then 
        return false
    end

    local function _update(des, src)
        for k, v in pairs(src) do
            if type(v) == 'table' then
                if not des[k] then des[k] = {} end
                _update(des[k], v)
            else
                des[k] = v
            end
        end
    end

    _update(self.server_list[server_id], server)

    return true
end

function ServerManager:getHallServerList()
    local list = {}
    for k,v in pairs(self.server_list) do 
        if v.server_type=="hallserver" then 
            table.insert(list, v)
        end
    end
    return list
end

function ServerManager:getGameServerList()
    local list = {}
    for k,v in pairs(self.server_list) do 
        if v.server_type=="gameserver" then 
            table.insert(list, v)
        end
    end
    return list
end

return ServerManager

