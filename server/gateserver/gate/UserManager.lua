--
-- @Author:      dreamfly
-- @DateTime:    2019-04-22 10:05:48
-- @Description: 用户信息管理

local log = require "Logger"
local UserInfo = require "UserInfo"

local UserManager = class("UserManager")


-- 构造函数
function UserManager:ctor()

	self.user_list = {}

end

-- 重置数据
function UserManager:reset()
    self.user_list = {}
end

-- 获取用户列表
function UserManager:getUserList()
	return self.user_list
end

--取玩家信息
function UserManager:getUserInfo(user_id)
	return self.user_list[user_id]
end

-- 查找用户信息
function UserManager:searchUserInfo(user_id)
    return self.user_list[user_id]
end

-- 创建用户信息
function UserManager:createUserInfo()
	return UserInfo.new()
end

--添加用户信息
function UserManager:addUserInfo(user_info)	
	if self.user_list[user_info.user_id] then
		log.debug("_______AddUserInfo__false___")
		return false
    end 
    	
	self.user_list[user_info.user_id] = user_info
	return true
end

--重新加载玩家信息
function UserManager:setUserInfo(user_info)	
	if not self.user_list[user_info.user_id] then
		-- log.debug("_______setUserInfo_____")
		return false
    end 
    	
	self.user_list[user_info.user_id] = user_info
	return true
end

--移除用户信息
function UserManager:removeUserInfo(user_id)
    if not self.user_list[user_id] then
		log.debug("_______removeUserInfo__false___")
		return false
    end 
    log.debug("_______removeUserInfo_____")	
    self.user_list[user_id] = nil
    
	return true
end


return UserManager
