--
-- @Author:      name
-- @DateTime:    2018-03-30 23:05:48
-- @Description: 登录处理

local skynet = require "skynet"
local log = require "Logger"
local NodeMessage = require "NodeMessage"
local config = require "configquery"


local Login = class("Login")

--构造函数
function Login:ctor(message_dispatch, node_message, session_manager, user_manager)

	self.message_dispatch = message_dispatch
	self.node_message = node_message
	self.session_manager = session_manager
	self.user_manager = user_manager

	self:register()
end

--注册消息
function Login:register()	

	self.message_dispatch:registerSelf("userLoginAuthReq", handler(self,self.onUserLoginAuth))
	self.message_dispatch:registerSelf("userLogoutReq", handler(self,self.onUserLogout))

end

--登录token认证消息
function Login:onUserLoginAuth(msg_data)

	local user_id = msg_data.user_id
	local token = msg_data.token
	local session_id = msg_data.session_id
	local login_result = {
		error_code = 0,
		error_msg = "登陆成功！",
	}

	print("_____登录认证消息_________Login:onUserLoginAuth",msg_data)

	local user_info = self.user_manager:getUserInfo(user_id)
	if not user_info then
		--从redis取信息
		local gate_info = self.node_message:callDbRedis("hgetall", { "gate_info:" .. user_id, }, user_id)
		print("_____getUserInfo___info___",gate_info)
		if next(gate_info) then 
			user_info = self.user_manager:createUserInfo()
			if user_info then
				user_info.user_id = tonumber(gate_info.user_id)
				user_info.token = gate_info.token	
				self.user_manager:addUserInfo(user_info)
			end
		else
			return {userLoginAuthRes={
				error_code = 1,
				error_msg = "登陆失败,取不到网关信息",
			}}
		end	
	end
	print("_____________user_info__",user_info)
	--验证
	if token and token ~= user_info:getToken()then
		local gate_info = self.node_message:callDbRedis("hgetall", { "gate_info:" .. user_id, }, user_id)
		if token == gate_info.token then 
			user_info.token = gate_info.token
		else
			return {userLoginAuthRes={
				error_code = 1,
				error_msg = "登陆失败！",
			}}		
		end
	end

	--局部（多个服务）
	local old_session_id = user_info:getSessionId()
	local new_session_id = session_id

	local logined = self.node_message:callProxy("check_login", user_id, session_id)
	if logined then
		--用户异地登陆
		self:redirectLoing(old_session_id, new_session_id)
	end
	-- end
	--更新时间
	user_info.update_time = os.time()
	user_info.session_id = new_session_id

	--绑定会话
	self.session_manager:bindSession(new_session_id, user_id)
	--通知manager进行user_id 与 gate_service绑定
	self.node_message:callProxy("user_login", user_id, session_id)

	-- print("_________通知hallserver____")
	--通知hallserver 用户进入
	-- local body = {
	-- 	user_id = user_id,
	-- }
	-- self.node_message:sendNodeServer(SERVER_TYPE.hall, 0, user_id, self.cmd.DMSG_MDM_HALL, self.cmd.DMSG_C2H_EnterHall, body)		

	return {userLoginAuthRes=login_result}
end

--异地登陆
function Login:redirectLoing(old_session_id, new_session_id)
	if not old_session_id then 
		return 
	end
	log.debug("________异地登录___",old_session_id, new_session_id)
	--发送异地登录通知
	local session = self.session_manager:getSession(old_session_id)
	if session then
		local body = {
			error_code = 0;
			error_msg = "用户在别的地方登录！"
		}
		self.node_message:sendClient(session.session_id, session.contype, "kickUserNt", body)
	end
	--解除绑定
	self.session_manager:unbindSession(old_session_id)
	--通知管理服踢人下线
	self.node_message:callProxy("kick_user", old_session_id)

	return true
end



--玩家登出
function Login:onUserLogout(msg_data)
	local user_id = msg_data.user_id
	local session_id = msg_data.session_id

	local user_id = msg_data.message_body.user_id
	local user_info = self.user_manager:searchUserInfo(user_id)
	if not user_info or not next(user_info) then 
		return 
	end
	log.debug("___玩家登出___",user_id)
	self.session_manager:unbindSession(session_id)
	--通知manager进行user_id 与 gate_service绑定
	self.node_message:callProxy("user_logout", user_id, session_id)	
end


return Login