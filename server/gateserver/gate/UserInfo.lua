
--
-- @Author:      dreamfly
-- @DateTime:    2019-04-22 10:05:48
-- @Description: 用户信息


local skynet = require "skynet"
local log = require "Logger"


local UserInfo = class("UserInfo")


-- 初始化
function UserInfo:ctor()

    self.user_id = 0
    self.user_status = 0
    self.token = ""
    self.login_server_id = 0
    self.hall_server_id = 0
    self.game_server_id = 0
    self.zone_server_id = 0
    self.session_id = nil
end

-- 重置数据
function UserInfo:reset()

    self.user_id = 0
    self.user_status = 0
    self.token = ""
    self.update_time = 0
    self.login_server_id = 0
    self.hall_server_id = 0
    self.game_server_id = 0
    self.zone_server_id = 0
    self.session_id = nil
end

-- 获取用户标识
function UserInfo:getUserId()
    return self.user_id
end

-- 获取用户状态
function UserInfo:getUserStatus()
    return self.user_status
end

-- 获取token
function UserInfo:getToken()
    return self.token
end

-- 获取更新时间
function UserInfo:getUpdateTime()
    return self.update_time
end


-- 获取大厅服务器标识
function UserInfo:getHallServerId()
    return self.hall_server_id
end

-- 获取游戏服务器标识
function UserInfo:getGameServerId()
    return self.game_server_id
end

-- 获取区域服务器标识
function UserInfo:getZoneServerId()
    return self.zone_server_id
end

-- 获取网络会话标识
function UserInfo:getSessionId()
    return self.session_id
end

return UserInfo
