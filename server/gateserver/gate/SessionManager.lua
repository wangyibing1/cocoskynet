--
-- @Author:      dreamfly
-- @DateTime:    2019-04-23 10:05:48
-- @Description: 会话管理

local skynet = require "skynet"
local log = require "Logger"


local SessionManager = class("SessionManager")


--初始化
function SessionManager:ctor()
    
    -- local session = {
    --     session_id = nil,    --socket句柄
    --     contype = nil,       --socket类型
    --     service = nil,       --socket服务
    --     addr = nil,          --ip地址
    --     time = nil,          --计时器，长时间没反应删除socket
    --     sid = nil,           --消息验证id
    --     user_id = nil,       --用户id
    -- }

    self.session_list = {}   --存储用户标识user_id
end


--重置数据
function SessionManager:reset()

    self.session_list = {}
end

--添加会话
function SessionManager:addSession(session)

    self.session_list[session.session_id] = session
end

--移除会话
function SessionManager:removeSession(session_id)
    if self.session_list[session_id] then
        self.session_list[session_id] = nil
        return true
    end

    return false
end

--是否验证
function SessionManager:isAuthSession(session_id)
    if self.session_list[session_id] then
        if self.session_list[session_id].user_id and self.session_list[session_id].user_id > 0 then
            return true
        end
    end

    return false
end

--绑定会话
function SessionManager:bindSession(session_id, user_id)

    if self.session_list[session_id] then
        self.session_list[session_id].user_id = user_id
        return true
    end

    return false
end

--解除绑定会话
function SessionManager:unbindSession(session_id)

    if self.session_list[session_id] then
        self.session_list[session_id].user_id = nil
        return true
    end

    return false
end

--获取所有会话
function SessionManager:getSessionList()
    return self.session_list
end

--获取会话
function SessionManager:getSession(session_id)
    return self.session_list[session_id]
end

--查找会话
function SessionManager:searchSession(session_id)
    return self.session_list[session_id]
end




return SessionManager