--
-- @Author:      name
-- @DateTime:    2018-03-30 23:05:48
-- @Description: 节点内消息的处理

local skynet = require "skynet"
local log = require "Logger"
local config = require "configquery"
local ProtoLoader = require "ProtoLoader"

local Network = require "Network"
print("__________Network__________",type(Network))

local MessageHandler = class("MessageHandler")

---------------------------------------------------------
-- Private
---------------------------------------------------------
function MessageHandler:ctor(message_dispatch, node_message)

	self.server_id = tonumber(skynet.getenv("server_id")) --节点名
	self.proto_file = skynet.getenv("proto_config") --proto文件路径	
	self.message_dispatch = message_dispatch	
	self.node_message = node_message
	-- self.proto_loader = ProtoLoader.new() --proto加载
	self.debug_port = tonumber("29"..self.server_id)


	self:register()
	
end

--注册本服务里的消息
function MessageHandler:register()

	self.message_dispatch:registerSelf('start',handler(self,self.start))
	
end

---------------------------------------------------------
-- CMD
---------------------------------------------------------
function MessageHandler:start()

	--数据库,加入需要的表
	local tables = {
		{name="user", pk='user_id', no_use_redis=true },		
	}
 	self.node_message:callDbProxy("load_tables",tables)	
 	-- local res = self.node_message:callDbProxy("get_user", nil, {{user_id=1}})
 	-- print("__________res___",res)

	local web = skynet.newservice("web_service")
	skynet.call(web,"lua","start")

	

	--console
	local console = skynet.uniqueservice("console_service",self.debug_port)
	-- local res = self.node_message:callService(console, "list")
	-- skynet.sleep(100)
	-- local network = Network.new("127.0.0.1", self.debug_port)
	-- skynet.sleep(200)
	-- network:writePackage("\n")
	-- network:writePackage("list\n")
	-- print("_____res___", res)

end






return MessageHandler