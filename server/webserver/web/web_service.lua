--
-- http://172.16.168.168:8001/page/login/login.html


local skynet = require "skynet"
local socket = require "skynet.socket"

local table = table
local string = string
local file_caches = {}

local function start()
	local agent = {}
	for i= 1, 20 do
		agent[i] = skynet.newservice("agent_service", i)
	end
	local balance = 1
	local id = socket.listen("0.0.0.0", 8001)
	skynet.error("Listen web port 8001")
	local function start_callback(id, addr)
		skynet.error(string.format("%s connected, pass it to agent :%08x", addr, agent[balance]))
		skynet.send(agent[balance], "lua", id)
		balance = balance + 1
		if balance > #agent then
			balance = 1
		end
	end
	socket.start(id, start_callback)

end



skynet.start(function(...)
	--监听lua消息
	skynet.dispatch("lua", function (_,_, cmd,key,value)
		if cmd == "get" then
			local v = file_caches[key]
			skynet.ret(skynet.pack(v))
		elseif cmd == "set" then
			file_caches[key] = value
			skynet.ret(skynet.pack(true))
		elseif cmd == "start" then		
			start()
			skynet.ret(skynet.pack(true))
		else
			print("not support :",cmd)
		end
	end)
end)