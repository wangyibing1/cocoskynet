require "skynet.manager"
local skynet = require "skynet"
local NodeMessage = require "NodeMessage"
local MessageDispatch = require "MessageDispatch"
local MessageHandler = require "web.MessageHandler"
local ClientHandler = require "web.ClientHandler"


local param = {...}
local agent_num = param[1]

global = {}

local function init()

	local message_dispatch = MessageDispatch.new()	
	local node_message = NodeMessage.new()
	local message_handler = MessageHandler.new(message_dispatch, node_message)
	local client_handler = ClientHandler.new(message_dispatch, node_message)
	

	skynet.dispatch("lua", handler(message_handler,message_handler.recevie))
end
---------------------------------------------------------
-- skynet
---------------------------------------------------------

skynet.start(function()

	init()
	skynet.register('.agent_service'..agent_num)
end)
