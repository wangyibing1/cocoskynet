--
-- @Author:      name
-- @DateTime:    2018-03-30 23:05:48
-- @Description: 客户端消息的处理

local skynet = require "skynet"
local log = require "Logger"
local config = require "configquery"



local ClientHandler = class("ClientHandler")

---------------------------------------------------------
-- Private
---------------------------------------------------------
function ClientHandler:ctor(message_dispatch, node_message)

	self.message_dispatch = message_dispatch	
	self.node_message = node_message

	self:register()
end

--注册本服务里的消息
function ClientHandler:register()

	self:registerSelf('login', "onLogin")
	self:registerSelf('list', "onList")
end

function ClientHandler:registerSelf(message_name, callback)
	if not callback then 
		callback = message_name
	end	
	if not self[callback] then 
		log.error("__registerSelf_没有注册函数回调____",message_name)
	end
	self.message_dispatch:registerSelf(message_name, handler(self,self[callback]))
end

---------------------------------------------------------
-- CMD
---------------------------------------------------------

function ClientHandler:onLogin( data )
	print("____onlogin__",data)
	local data = data.content
	local username = data.name
	local password = data.password
	local tbname = "user"
 	local res = self.node_message:callDbMySqlEx("exists", tbname, nil, {{username=username},{password=password}})
 	-- print("__________res___",res)	
	if res and next(res) then 
		local token = username..":"..os.time()
		local redis_key = "token:"..token
		self.node_message:callDbRedis("hmset", {redis_key, {username=username} })
		self.node_message:callDbRedis("expire", {redis_key, 60 })
		
		return {
			status="success",
			token = token,
		}
	end
	return {status="error", msg="登录失败,账号密码不正确!"}
end

function ClientHandler:onList( data )
	print("____onList__",data)

	local res = self.node_message:callService(".console", "list")
	local data = {}
	for k, v in pairs(res) do 
		local item = {}
		item.newsId = k
		item.newsName = v
		item.abstract= "44444"
		item.newsStatus= "0"
		item.newsImg="../../images/userface1.jpg"
		item.newsLook= "开放浏览"
		item.newsTop= ""
		item.newsTime= "2017-04-14 00:00:00"
		item.content = "css3用transition实现边框动画效果<img src=	"
		table.insert(data, item)
	end

	return {

		status="success", 
		code= 0,
		msg= "",
		count= 15,
		data= data,		
	}

end

return ClientHandler