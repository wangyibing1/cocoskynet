layui.use(['form','layer','jquery'],function(){
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer
        $ = layui.jquery;

    $(".loginBody .seraph").click(function(){
        layer.msg("这只是做个样式，至于功能，你见过哪个后台能这样登录的？还是老老实实的找管理员去注册吧",{
            time:5000
        });
    })
    //自定义验证规则
    form.verify({
        userName: function(value){
          if(value.length < 5){
            return '用户名至少得5个字符啊';
          }
        },

        password: [
          /^[\S]{3,12}$/
          ,'密码必须3到12位，且不能出现空格'
        ]

    });

    //登录按钮
    form.on("submit(login)",function(data){
        // $(this).text("登录中...").attr("disabled","disabled").addClass("layui-disabled");
        // setTimeout(function(){
        //     window.location.href = "/layuicms2.0";
        // },1000);
        // 
        var currentCookie = document.cookie
        console.log("______data___",JSON.stringify(data.field), currentCookie)  
        var userInfo = JSON.stringify(data.field);
        var url = "11login?content="+userInfo;
        $.ajax({
            url:url,
            type:'get',
            data:userInfo,
            dataType:'json',
            headers:{'token':currentCookie},

            beforeSend:function () {
                // this.layerIndex = layer.load(0, { shade: [0.5, '#393D49'] });
                console.log("__________2_______")
            },
            success:function(data){
                console.log("________success_________",data)
                // var data = JSON.parse(data)
                if(data.status == 'error'){                                       
                    layer.msg(data.msg,{icon: 5});//失败的表情
                    return;
                }else if(data.status == 'success'){                    
                    document.cookie = data.token
                    console.log("_________________",document.cookie)
                    layer.msg("登录成功!")
                    // window.location.href = "/layuicms2.0/index.html";
                }
            },
            complete: function () {
                // layer.close(this.layerIndex);
                console.log("__________5_______")
            },
        });
        return false;
    })

    //表单输入效果
    $(".loginBody .input-item").click(function(e){
        e.stopPropagation();
        $(this).addClass("layui-input-focus").find(".layui-input").focus();
    })
    $(".loginBody .layui-form-item .layui-input").focus(function(){
        $(this).parent().addClass("layui-input-focus");
    })
    $(".loginBody .layui-form-item .layui-input").blur(function(){
        $(this).parent().removeClass("layui-input-focus");
        if($(this).val() != ''){
            $(this).parent().addClass("layui-input-active");
        }else{
            $(this).parent().removeClass("layui-input-active");
        }
    })
})
