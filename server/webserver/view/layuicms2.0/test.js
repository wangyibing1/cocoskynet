layui.use(['form','layer','laydate','table','laytpl'],function(){
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        laydate = layui.laydate,
        laytpl = layui.laytpl,
        table = layui.table;

    var data = null;
    //新闻列表
    var tableIns = {
        elem: '#newsList',
        // url : '../../json/newsList.json',
        // url="/list?content=1",
        cellMinWidth : 95,
        page : true,
        height : "full-125",
        limit : 20,
        limits : [10,15,20,25],
        id : "newsListTable",
        cols : [[
            {type: "checkbox", fixed:"left", width:50},
            {field: 'newsId', title: '服务ID', align:"center"},
            {field: 'newsName', title: '服务名', width:350},
            {field: 'newsAuthor', title: '发布者', align:'center'},
            {field: 'newsStatus', title: '发布状态',  align:'center',templet:"#newsStatus"},
            {field: 'newsLook', title: '浏览权限', align:'center'},
            {field: 'newsTop', title: '是否置顶', align:'center', templet:function(d){
                return '<input type="checkbox" name="newsTop" lay-filter="newsTop" lay-skin="switch" lay-text="是|否" '+d.newsTop+'>'
            }},
            {field: 'newsTime', title: '发布时间', align:'center', minWidth:110, templet:function(d){
                return d.newsTime.substring(0,10);
            }},
            {title: '操作', width:170, templet:'#newsListBar',fixed:"right",align:"center"}
        ]],
        data : []
    }

    //是否置顶
    form.on('switch(newsTop)', function(data){
        var index = layer.msg('修改中，请稍候',{icon: 16,time:false,shade:0.8});
        setTimeout(function(){
            layer.close(index);
            if(data.elem.checked){
                layer.msg("置顶成功！");
            }else{
                layer.msg("取消置顶成功！");
            }
        },500);
    })



    //搜索【此功能需要后台配合，所以暂时没有动态效果演示】
    $(".search_btn").on("click",function(){
        
        var url = "list?content=1";
        $.ajax({
            url:url,
            type:'get',
            data:1,
            dataType:'json',
            headers:{'token':1},

            beforeSend:function () {
                // this.layerIndex = layer.load(0, { shade: [0.5, '#393D49'] });
                console.log("__________2_______")
            },
            success:function(data){
                // console.log("________success_________",data)
                // var data = JSON.parse(data)
                if(data.status == 'error'){                                       
                    layer.msg(data.msg,{icon: 5});//失败的表情
                    return;
                }else if(data.status == 'success'){                    
                    
                    console.log("_______success__________",data)
                    layer.msg("登录成功!")
                    var itemData = data.data
                    $.extend(tableIns, {data: itemData});
                    console.log(tableIns);
                    table.reload("newsListTable", tableIns);  
                    data=null;                   
                }
            },
            complete: function () {
                // layer.close(this.layerIndex);
                console.log("__________5_______")
            },
        });
    });


 // 表格初始化 ------------------
    function init (){         
        console.log("__________5555555_______")                                             

        var url = "list?content=1";
        $.ajax({
            url:url,
            type:'get',
            data:1,
            dataType:'json',
            headers:{'token':1},
            success:function(data){
                if(data.status == 'success'){                    
                    console.log("_______success__________",data)
                    layer.msg("登录成功!")
                    var itemData = data.data
                    $.extend(tableIns, {data: itemData});
                    console.log(tableIns);
                    table.render(tableIns);  
                    data=null;                   
                }
            },
        });         

              
        data=null;               
    }
    // 页面初始化---------------------------------
    init();

})