--
-- @Author:      dreamfly
-- @DateTime:    2019-04-19 14:02:03
-- @Description: 房间管理


local RoomManager = class("RoomManager")


--构造函数
function RoomManager:ctor()

    self.room_config = {} 
    self.desk_list = {}
end


--重置数据
function RoomManager:reset()
    self.room_config = {} 
    self.desk_list = {}
end

--设置房间配置
function RoomManager:setRoomConfig(room_config)
    self.room_config = room_config
end

--获取房间配置
function RoomManager:getRoomConfig()
    return self.room_config
end

function RoomManager:getGameIdByDesk(desk_id)
    local str = tostring(desk_id)
    str = string.sub(str,9,10)
    return tonumber(str)
end

function RoomManager:getRoomIdByDesk(desk_id)
    local str = tostring(desk_id)
    str = string.sub(str,11,11)
    return tonumber(str)
end

--通过桌子id取房间配置
function RoomManager:getRoomConfigByDesk(desk_id)
    local room_id = self:getRoomIdByDesk(desk_id)
    if self.room_config[room_id] then 
        return self.room_config[room_id]
    end
    return nil
end


--添加桌子
function RoomManager:addDesk(desk)
    self.desk_list[desk.desk_id] = desk
end

--更新桌子
function RoomManager:updateDesk(desk)
    local desk_id = desk.desk_id
    if self.desk_list[desk_id] then

        local function _update(des, src)
            for k, v in pairs(src) do
                if type(v) == 'table' then
                    if not des[k] then des[k] = {} end
                    _update(des[k], v)
                else
                    des[k] = v
                end
            end
        end

        _update(self.desk_list[desk_id], desk)
    end
end

--移除桌子
function RoomManager:removeDesk(desk_id)
    if self.desk_list[desk_id] then
        self.desk_list[desk_id] = nil
        return true
    else
        return false
    end
end

--清理桌子
function RoomManager:clearDesk()
    self.desk_list = {}
end

--获取桌子
function RoomManager:getDesk(desk_id)
    -- print("____self.desk_list___",self.desk_list, desk_id)
    return self.desk_list[desk_id]
end

--查找桌子
function RoomManager:searchDesk(desk_id)
    return self.desk_list[desk_id]
end

--获取桌子列表
function RoomManager:getDeskList()
    return self.desk_list
end

--获取桌子数目
function RoomManager:getDeskCount()
    return #self.desk_list
end

return RoomManager

