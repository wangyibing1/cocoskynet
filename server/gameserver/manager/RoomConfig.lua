--
-- @Author:      dreamfly
-- @DateTime:    2019-05-05 14:02:03
-- @Description: 房间配置

local cjson = require "cjson"

local RoomConfig = class("RoomConfig")

-- 初始化
function RoomConfig:ctor()
    self.server_id = 0      --服务器标识
    self.server_type = 0    --服务器类型
    self.server_name = ""   --服务器名称

    self.game_id = 0        --游戏id
    self.group_id = 0       --分组id
    self.game_name = ""     --游戏名称
    self.game_code = ""     --游戏代码
    self.game_genre = 0     --游戏类型

    self.category_id = 0 --游戏种类id
    self.category_name = "" --游戏种类名
    self.category_code = "" --游戏种类代号
    self.group_name = ""

    self.dynamic_join = false   --动态加入
    self.offline_trusteeship = true --断线代打

    self.server_rule = 0    --规则

    self.desk_count = 1     --桌子数
    self.seat_count = 1     --椅子数
    self.player_count = 1   --单桌人数上限

    self.base_score = 1     --底分
    self.revenue_ratio = 1  --税收比
    self.service_cost = 0   --服务费

    self.min_enter_score = 0  --最低进入积分
    self.max_enter_score = 0  --最高进入积分
    self.restrict_score = 0   --限制积分
    self.min_game_score = 0   --游戏积分

    self.min_enter_member = 0  --最小进入人数
    self.max_enter_member = 0  --最大进入人数

    self.game_config = {} --游戏配置
end

--获取数据
function RoomConfig:getData()
    return {
        server_id = self.server_id,
        server_type = self.server_type,
        server_name = self.server_name,

        game_id = self.game_id,
        group_id = self.group_id,
        game_name = self.game_name,
        game_code = self.game_code,
        game_genre = self.game_genre,

        category_id =self.category_id,
        category_name =self.category_name,
        category_code =self.category_code,
        group_name = self.group_name,

        dynamic_join = self.dynamic_join,
        offline_trusteeship = self.offline_trusteeship,

        server_rule = self.server_rule,

        desk_count = self.desk_count,
        seat_count = self.seat_count,
        player_count = self.player_count,

        base_score = self.base_score,
        revenue_ratio = self.revenue_ratio,
        service_cost = self.service_cost,

        min_enter_score = self.min_enter_score,
        max_enter_score = self.max_enter_score,
        restrict_score = self.restrict_score,
        min_game_score = self.min_game_score,

        min_enter_member = self.min_enter_member,
        max_enter_member = self.max_enter_member,

        game_config = cjson.encode(self.game_config),

    }
end

--初始化配置
function RoomConfig:init(config)
    self.server_id = config.server_id --服务器标识
    self.sever_type = config.type --服务器类型
    self.server_name = config.name --服务器名

    self.game_id = config.game_id        --游戏id
    self.group_id = config.group_id or 1 --分组id
    self.game_name = config.game_name     --游戏名称
    self.game_code = config.game_code     --游戏代码
    self.game_genre = config.game_genre     --游戏类型

    self.category_id = config.category_id or 0
    self.category_name = config.category_name or ""
    self.category_code = config.category_code or ""
    self.group_name = config.group_name or ""

    self.dynamic_join = config.dynamic_join   --是否动态加入
    self.offline_trusteeship = config.offline_trusteeship  --断线代打
    self.server_rule = config.server_rule or 0   --规则
    self.desk_count = config.desk_count or 1     --桌子数
    self.seat_count = config.seat_count or 1     --椅子数
    self.base_score = config.base_score or 1     --底分
    self.player_count = config.player_count or 1     --单桌人数上限
    self.revenue_ratio = config.revenue_ratio or 1  --税收比
    self.service_cost = config.service_cost or 0   --服务费
    self.min_enter_score = config.min_enter_score or 0  --最低进入积分
    self.max_enter_score = config.max_enter_score or 0  --最高进入积分
    self.restrict_score = config.restrict_score or 0   --限制积分
    self.min_game_score = config.min_game_score or 0   --游戏积分
    self.min_enter_member = config.min_enter_member or 0  --最小进入人数
    self.max_enter_member = config.max_enter_member or 0  --最大进入人数
    self.game_config = cjson.decode(config.game_config or "{}") --游戏配置
end

--获取服务器标识
function RoomConfig:getServerId()
    return self.server_id
end

--获取服务器类型
function RoomConfig:getServerType()
    return self.server_type
end

--获取服务器名称
function RoomConfig:getServerName()
    return self.server_name
end

--获取游戏标识
function RoomConfig:getGameId()
    return self.game_id
end

--获取分组标识
function RoomConfig:getGroupId()
    return self.group_id
end

function RoomConfig:getGroupName()
    return self.group_name
end

--获取游戏名称
function RoomConfig:getGameName()
    return self.game_name
end

--获取游戏代码
function RoomConfig:getGameCode()
    return self.game_code
end

--获取游戏类型
function RoomConfig:getGameGenre()
    return self.game_genre
end

--获取游戏种类id
function RoomConfig:getCategoryId()
    return self.category_id
end

--获取游戏种类名
function RoomConfig:getCategoryName()
    return self.category_name
end

--获取游戏种类代号
function RoomConfig:getCategoryCode()
    return self.category_code
end

--是否动态加入
function RoomConfig:isDynamicJoin()
    return self.dynamic_join
end

--是否断线代打
function RoomConfig:isOfflineTrusteeship()
    return self.offline_trusteeship
end

--获取服务器规则
function RoomConfig:getServerRule()
    return self.server_rule
end

--获取桌子数目
function RoomConfig:getDeskCount()
    return self.desk_count
end

--获取座位数目
function RoomConfig:getSeatCount()
    return self.seat_count
end

--取桌子人数上限
function RoomConfig:getPlayerCount()
    return self.player_count
end

--游戏底分
function RoomConfig:getBaseScore()
    return self.base_score
end

--税收比例
function RoomConfig:getRevenueRatio()
    return self.revenue_ratio
end

--服务费用
function RoomConfig:getServiceCost()
    return self.service_cost
end

--最低进入积分
function RoomConfig:getMinEnterScore()
    return self.min_enter_score
end

--最高进入积分
function RoomConfig:getMaxEnterScore()
    return self.max_enter_score
end

--限制积分
function RoomConfig:getRestrictScore()
    return self.restrict_score
end

--最低游戏积分
function RoomConfig:getMinGameScore()
    return self.min_game_score
end

--最低进入会员
function RoomConfig:getMinEnterMember()
    return self.min_enter_member
end

--最高进入会员
function RoomConfig:getMaxEnterMember()
    return self.max_enter_member
end

--获取游戏配置
function RoomConfig:getGameConfig()
    return self.game_config
end

return RoomConfig





