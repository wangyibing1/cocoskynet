--
-- @Author:      name
-- @DateTime:    2019-04-28 23:05:48
-- @Description: 服务器管理


local ServerManager = class("ServerManager")

--初始化
function ServerManager:ctor()
    -- local server = {
    --     server_id = 0,
    --     server_type = 0,
    --     cluster_addr = "",
    --     server_name = "",
    --     cluster_id = "",
    -- }
    self.max_client = 1024 --设置最大客户端
    self.client_count = 0
    self.online_count = 0
    self.server_list = {}  
end

--设置最大客户端
function ServerManager:setMaxClient(max_client)
    max_client = max_client or 1024
    if max_client <= 0 then
        max_client = 1
    end

    self.max_client = max_client
end

--获取最大客户端
function ServerManager:getMaxClient()
    return self.max_client
end

--获取客户端数目
function ServerManager:getClientCount()
    return self.client_count
end

--获取在线数目
function ServerManager:getOnlineCount()
    return self.online_count
end

--增加在线数目
function ServerManager:increaseOnlineCount(count)
    count = count or 1
    self.online_count = self.online_count + count
end

--减少在线数目
function ServerManager:decreaseOnlineCount(count)
    count = count or 1
    if self.online_count - count >= 0 then
        self.online_count = self.online_count - count
    else
        self.online_count = 0
    end
end

--添加服务器信息
function ServerManager:addServer(server)
    local server_id = server.server_id

    self.server_list[server_id] = server
end

--移除服务器信息
function ServerManager:removeServer(server_id)

    if self.server_list[server_id] then 
        self.server_list[server_id] = nil
        return true
    end

    return false
end

--获取服务器信息
function ServerManager:getServer(server_id)
    return self.server_list[server_id]
end

--获取服务器列表
function ServerManager:getServerList()
    return self.server_list
end

--获取服务器信息
function ServerManager:getServerListByType(server_type)    
    local server_list = {}
    for _, v in pairs(self.server_list) do         
        if v.server_type == server_type then 
            table.insert(server_list, v)
        end
    end    

    return server_list
end

return ServerManager

