--
-- @Author:      
-- @DateTime:    2019-04-19 14:02:03
-- @Description: 用户对象管理
--  

local log = require "Logger"
local UserInfo = require "UserInfo"

local UserManager = class("UserManager")


-- 初始化
function UserManager:ctor(node_message)
    self.user_list = {}
    self.seat_list = {}
end

-- 重置数据
function UserManager:reset()
    self.user_list = {}
end

-- 获取用户列表
function UserManager:getUserList()
	return self.user_list
end

--取玩家信息
function UserManager:getUserInfo(user_id)
	return self.user_list[user_id]
end
function UserManager:getUser(user_id)
    return self.user_list[user_id]
end

-- 查找用户信息
function UserManager:searchUserInfo(user_id)
    return self.user_list[user_id]
end

-- 创建用户信息
function UserManager:createUserInfo(user_data)
	return UserInfo.new(user_data)
end

--添加用户信息
function UserManager:addUserInfo(user_info)	
    if not user_info.user_id then 
        log.error("______游戏服务添加user时发生错误___",user_info)
        return
    end
    local user = self.user_list[user_info.user_id]
	if user then	
		return true
    end     	
    self.user_list[user_info.user_id] = user_info
	return true
end

--更新用户信息
function UserManager:updateUserInfo(user_info)
    local user_id = user_info.user_id
    if self.user_list[user_id] then

        local function _update(des, src)
            for k, v in pairs(src) do
                if type(v) == 'table' then
                    if not des[k] then des[k] = {} end
                    _update(des[k], v)
                else
                    des[k] = v
                end
            end
        end
        _update(self.user_list[user_id], user_info)
    end
end

--重新加载玩家信息
function UserManager:setUserInfo(user_info)	
	if not self.user_list[user_info.user_id] then
		log.debug("_______setUserInfo_____")
		return false
    end 
    	
	self.user_list[user_info.user_id] = user_info
	return true
end

--移除用户信息
function UserManager:removeUserInfo(user_id)
	local user_info = self.user_list[user_id]
    if not user_info then
		log.debug("_______removeUserInfo_的玩家信息不存在____",user_info)
		return false
    end 	
    self:standUp(user_id)
    self.user_list[user_id] = nil
    print("__UserManager_user_id_删除_", user_id )
	return true
end

function UserManager:setAllUserValue(name, value)
    for user_id, user in pairs(self.user_list) do 
        user[name] = value
    end
end

function UserManager:setUserValue(user_id, name, value)
    local user = self.user_list[user_id]
    if user then 
        user[name] = value
    end
end

function UserManager:getPlayingUser()
    local users = {}
    for user_id, user in pairs(self.user_list) do 
        if user.playing then 
            users[user_id] = user
        end
    end
    return users
end
function UserManager:getSeatUser()
    local users = {}
    for user_id, user in pairs(self.user_list) do 
        local seat_id = user:getSeatId()
        if seat_id and seat_id>0 then 
            users[user_id] = user
        end
    end
    return users
end


--坐下
function UserManager:sitDown(user_id, seat_id)
    local seats = {}
    if seat_id and seat_id>0 then --指定座位
        local user_id = self.seat_list[seat_id] 
        if not user_id or user_id==0 then 
            table.insert(seats,seat_id)
        else
            print("---------座位上已经有人---------------",user_id, seat_id, self.seat_list)
            return false
        end
    else
        --找到空座位
        for id=1, 4 do 
            seat_id = self.seat_list[id]
            if not seat_id or seat_id == 0 then 
                table.insert(seats,id)
            end
        end
    end
    table.mix(seats) --随机位置    
    if #seats > 0 then 
        seat_id = seats[1]
    else
        print("---------没有座位了-------")
        return false
    end
    local user = self.user_list[user_id]
    if user then
        if user.seat_id and user.seat_id>0 then --玩家已经坐下了，要换位置
            self:standUp(user_id)
        end        
        self.seat_list[seat_id] = user_id
        user:setSeatId(seat_id)
    end
    return seat_id
end
--离座
function UserManager:standUp(user_id)
    if not user_id or user_id <=0 then
        return false
    end 
    local user = self.user_list[user_id]
    if not user then
        return false
    end
    local seatid = user:getSeatId()
    if seatid then 
        self.seat_list[seatid] = 0        
    end
    user.seatid = 0 
    return true
end



return UserManager

