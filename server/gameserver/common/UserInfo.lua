--
-- @Author:      
-- @DateTime:    2019-04-19 14:02:03
-- @Description: 用户对象处理

local log = require "Logger"

local UserInfo = class("UserInfo")

--构造函数
function UserInfo:ctor(user_data)
    -- print("_________USERINFO______",user_data)
    --表数据
    self.user_id = user_data.user_id	    --用户标识
    self.username = user_data.username 	   	--用户名
    self.password = user_data.password      --玩家密码
    self.online = user_data.online or 0 --(0.离线 1.在线)
    self.position = user_data.position or 0 --位置:1.hall大厅,2.room游戏
    self.status = user_data.status or 1 --状态（0锁定、1正常）
    self.nickname = user_data.nickname	or ''  	--用户昵称
    self.is_robot = user_data.is_robot == 1 or false
    self.gold = user_data.gold               --用户金币


    --游戏中数据
    self.playing = false
    self.seat_id = 0
    self.desk_id = 0
end


--新表结构
function UserInfo:getOpenId()return          self.openid    end
function UserInfo:getType()return            self.type end
function UserInfo:getOnline()return          self.online end
function UserInfo:getPosition()return        self.position end
function UserInfo:getGameId()return          self.game_id  end
function UserInfo:getRoundId()return         self.round_id end

function UserInfo:getRegisterTime()return            self.register_time end
function UserInfo:getLastLoginIp()return             self.last_login_ip end
function UserInfo:getLastLoginIpAddr()return         self.last_login_ip_addr end
function UserInfo:getLastLoginTime()return           self.last_login_time end
function UserInfo:getLastTransferTime()return        self.last_transfer_time end
function UserInfo:getDeviceType()return      self.device_type end
function UserInfo:getStatus()return          self.status end

function UserInfo:getWallet()
    return self.wallet
end


--创建钱包
function UserInfo:createWallet(wallet_data)
    self.wallet = UserWallet.new(wallet_data)
end

function UserInfo:getWalletId()
    return self.wallet:getWalletId()
end

--获取用户标识
function UserInfo:getUserId()
    return self.user_id
end

--获取用户昵称
function UserInfo:getNickname()
    return self.nickname
end

--获取用户昵称
function UserInfo:getUsername()
    return self.username
end

--获取用户头像标识
function UserInfo:getFaceId()
    return self.face_id
end

--获取用户头像地址
function UserInfo:getFaceUrl()
    return self.portrait
end

--获取用户登陆密码
function UserInfo:getLoginPassword()
    return self.password
end

--获取用户保险箱密码
function UserInfo:getSafeBoxPassword()
    return self.safe_box_password
end

--获取渠道标识
function UserInfo:getChannelId()
    return self.channel_id
end

--获取游戏等级
function UserInfo:getGameLevel()
    return self.game_level
end

--获取用户等级
function UserInfo:getGrade()
    return self.grade
end

--获取用户分数
function UserInfo:getScore()
    return self.score
end

--获取用户金币
function UserInfo:getGold()
    -- return self.wallet:getBalance()
    return self.gold
end

--获取用户金币
function UserInfo:setGold(gold)
    return self.wallet:setBalance(gold)
end

--获取用户钻石
function UserInfo:getDiamond()
    return self.diamond
end

--获取用户保险箱
function UserInfo:getSafeBox()
    return self.safe_box
end

--获取经验值
function UserInfo:getExperience()
    return self.experience
end

--获取胜利局数
function UserInfo:getWinCount()
    return self.win_count
end

--获取失败局数
function UserInfo:getLostCount()
    return self.lost_count
end

--获取和局局数
function UserInfo:getDrawCount()
    return self.draw_count
end

--获取逃跑局数
function UserInfo:getFleeCount()
    return self.flee_count
end

--获取游戏局数
function UserInfo:getGameCount()
    return self.win_count + self.lost_count + self.draw_count + self.flee_count
end

--获取用户胜率
function UserInfo:getWinRatio()
    local game_count = self:getGameCount()
    if game_count ~= 0 then
        return self.win_count * 10000 / game_count
    else
        return 0
    end
end

--获取用户输率
function UserInfo:getLostRatio()
    local game_count = self:getGameCount()
    if game_count ~= 0 then
        return self.lost_count * 10000 / game_count
    else
        return 0
    end
end

--获取用户和率
function UserInfo:getDrawRatio()
    local game_count = self:getGameCount()
    if game_count ~= 0 then
        return self.draw_count * 10000 / game_count
    else
        return 0
    end
end

--获取用户逃率
function UserInfo:getFleeRatio()
    local game_count = self:getGameCount()
    if game_count ~= 0 then
        return self.flee_count * 10000 / game_count
    else
        return 0
    end
end

--获取桌子标识
function UserInfo:getDeskId()
    return self.desk_id
end
function UserInfo:setDeskId(desk_id)
    self.desk_id = desk_id
end

--获取座位标识
function UserInfo:getSeatId()
    return self.seat_id
end
function UserInfo:setSeatId(seat_id)
    self.seat_id = seat_id
end

function UserInfo:getPlaying()
    return self.playing
end

--获取用户状态
function UserInfo:getUserStatus()
    return self.user_status
end

--是否机器人用户
function UserInfo:isRobotUser()
    return self.is_robot
end

--获取总游戏得分
function UserInfo:getTotalGameScore()
    return self.total_game_score
end

--获取用户游戏规则
function UserInfo:getGameRule()
    return self.game_rule
end

--设置用户游戏规则
function UserInfo:setGameRule(game_rule)
    for k, v in pairs(game_rule) do
        self.game_rule[k] = v
    end
end




return UserInfo