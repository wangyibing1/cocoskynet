
local skynet = require("skynet")

local DeskBase = class("DeskBase")

function DeskBase:ctor()
	self.handlerStateTime = nil --定时器句柄
	self.timer = nil --定时器
    self.node_cluster_id = skynet.getenv("node_cluster_id")
    self.zone_cluster_id = skynet.getenv("zone_cluster_id")

end

--状态定时器
function DeskBase:setTimer(time, callback, loop, param)
	if not time then
		--没设置时间
		log.error("___定时器没设置时间___")		
		return 
	end	
	if self.handlerStateTime then 
		self.timer:unregister(self.handlerStateTime)
		self.handlerStateTime = nil	
	end
	self.handlerStateTime = self.timer:register(time, callback, loop, param)	
end

--关闭定时器
function DeskBase:stopTimer()
	if self.handlerStateTime then 
		self.timer:unregister(self.handlerStateTime)
		self.handlerStateTime = nil	
	end
end

--剩余时间
function DeskBase:getLeftTime()
	return self.timer:leftTime(self.handlerStateTime)
end

--给指定玩家发消息
function DeskBase:sendToUser(user_id,data)
	--判断玩家是否在线
	for k, v in pairs(data) do 
		self.node_message:sendNodeProxy(self.node_cluster_id, 'send_to_user', user_id, k, v)
	end
end

--
--给桌子所有玩家广播消息
function DeskBase:sendToAllUser(data)
	local user_list = self.user_manager:getUserList()
	local users = {}
	for user_id,v in pairs(user_list) do 
		--判断是否在线
		table.insert(users,user_id)		
	end
	if next(users) then
		--让nodeserver转发
		for k, v in pairs(data) do 
			self.node_message:sendNodeProxy(self.node_cluster_id, 'broadcast_to_user', users, k, v)		
		end
	end
end

--给别的玩家发消息
function DeskBase:sendToOther(my_id,data)
	local user_list = self.user_manager:getUserList()
	local users = {}
	for user_id,v in pairs(user_list) do 
		--判断是否在线
		if user_id~=my_id then 
			table.insert(users,user_id)		
		end
	end
	if next(users) then
		--让nodeserver转发
		for k, v in pairs(data) do 
			self.node_message:sendNodeProxy(self.node_cluster_id, 'broadcast_to_user', users, k, v)		
		end
	end
end

--给无座玩家发消息
function DeskBase:sendToNoSeat(data)
	local user_list = self.user_manager:getUserList()
	local users = {}
	for user_id,v in pairs(user_list) do 
		--判断是否在线
		--判断是否有座位
		-- if user_id~=my_id then 
			table.insert(users,user_id)		
		-- end
	end
	if next(users) then
		--让nodeserver转发
		for k, v in pairs(data) do 
			self.node_message:sendNodeProxy(self.node_cluster_id, 'broadcast_to_user', users, k, v)		
		end
	end
end

--清除离线的玩家
function DeskBase:removeDisconnect()
	local user_list = self.user_manager:getUserList()	
	for user_id, user in pairs(user_list) do 
		-- print("----------checkUser ", v.chip)
		if not user:getOnline() then		 		 
			--设置玩家所在桌子id为0	
			--更新玩家信息中所在桌子id
			--站起
			--更新桌子信息到nodeserver
			--删除user
			-- self:sendGate(v, 'update_tableid', 0)
			-- self:sitUp(k)
			self.user_manager:removeUserInfo(user_id)
			-- self:updatePlayerNum()
			print("拼十踢走掉线#######",k)
		end
	end
end

--当前状态
function DeskBase:getState()
	return self.fsm:get()
end

--状态对应的数字
function DeskBase:getStateNum()
	local state = self:getState()
	for k, v in pairs(self.stateTable) do 
		if state == v[1] then 
			return k
		end
	end
	return 0
end

--当前操作所剩余的时间
function DeskBase:getOperTime()
	local curState = self:getState()
	if not curState or not self.stateTime[curState] then 
		return 0
	end
	local time = self.stateTime[curState] - (os.time() - self.operStartTime)
	if time < 0 then 
		time = 0
	end
	return time
end

return DeskBase