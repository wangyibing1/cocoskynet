--
-- @Author:      name
-- @DateTime:    20189-07-30 23:05:48
-- @Description: 所有游戏桌子共用到的消息

local skynet = require "skynet"
local log = require "Logger"
local config = require "configquery"
local RoomConfig = require "RoomConfig"
-- require "common"

-- local AssertEx = AssertEx


local DeskMessage = class("DeskMessage")

---------------------------------------------------------
-- Private
---------------------------------------------------------
function DeskMessage:ctor(message_dispatch)
	self.message_dispatch = message_dispatch
	self.room_config = nil --房间配置
	self:register()
end

--注册本服务里的消息
function DeskMessage:register()
	self.message_dispatch:registerSelf('init', handler(self,self.init))	
    self.message_dispatch:registerSelf('enterDeskReq', handler(self,self.enterDeskReq))	
    self.message_dispatch:registerSelf('leaveDeskReq', handler(self,self.leaveDeskReq))	    
    self.message_dispatch:registerSelf('deskStatusReq', handler(self,self.deskStatusReq))	
    self.message_dispatch:registerSelf('sitDownReq', handler(self,self.sitDownReq))	
    self.message_dispatch:registerSelf('standUpReq', handler(self,self.standUpReq))	
    self.message_dispatch:registerSelf('cotrolReq', handler(self,self.cotrolReq))	

    self.message_dispatch:registerSelf('disconnect', handler(self,self.disconnect))	
end







---------------------------------------------------------
-- CMD
---------------------------------------------------------
--服务初始化
function DeskMessage:init(room_config, desk_config)
	self.room_config = global.room_config	
	self.node_message = global.node_message
	self.user_manager = global.user_manager
	global.desk_config = desk_config
	self.desk_id = global.desk_config.desk_id
	self.desk = global.desk

	self.room_config:init(room_config) --房间配置
	self.message_dispatch:dispatchSelfMessage("desk_start") --通知MessageHandler
end

--进入桌子
function DeskMessage:enterDeskReq(msg_data)
	print("____enterDeskReq____",msg_data)

    local user_id = msg_data.user_id    
    local user_info = self.user_manager:getUserInfo(user_id)
    if user_info then --已经在这个桌子里
    	--断线重连进来
    	print("_____断线重连进来的________")
		return {enterDeskRes={
			error_code = 0,
			error_msg = "进入桌子成功了",
			desk_id = user_info:getDeskId(),
		}}
    end

	--第一次进入桌子
    --加载用户信息
    local user_data = self.node_message:callDbProxy("get_user", nil,{{user_id=user_id}})
    print("___用户进入桌子___USER____",user_data)
    if user_data and next(user_data) then
        --添加用户
        user_info = self.user_manager:createUserInfo(user_data)
    else
        log.error("____enterDeskReq, 获取user失败", msg_data)
		return {enterDeskRes={
			error_code = 1,
			error_msg = "进入桌子失败，获取user失败",			
		}}
    end
    
    local room_config = self.room_config
    --比较用户信息与桌子配置信息
    local gold = user_info:getGold()
    if gold < room_config:getMinEnterScore() then 
        --小入场分数不得入场所
        return {enterDeskRes={
            error_code = 1,
            error_msg = "入场分数不足",
        }}
    end
    --桌子是否已满人  
    
    --入桌成功
    user_info:setDeskId(msg_data.desk_id)
    self.user_manager:addUserInfo(user_info)
    --符合条件，自动坐下
    self.user_manager:sitDown(user_id)

    -- --在线人数+1
    -- self.server_manager:increaseOnlineCount()
    -- --发送在线信息
    -- self:sendServerInfo()
    if self.desk.enterDeskCallback then 
    	self.desk:enterDeskCallback(user_id)
    end
	return {enterDeskRes={
		error_code=0,
		error_msg="进入桌子成功了",
		desk_id = user_info:getDeskId(),
	}}
end

--离开
function DeskMessage:leaveDeskReq(msg_data)
    local user_id = msg_data.user_id    
    local user_info = self.user_manager:getUserInfo(user_id)
    if not user_info then --玩家不在这个桌子里
    	log.error("_____调用离桌子，但玩家不在这个桌子里________",msg_data)
		return {leaveDeskRes={
			error_code=1,
			error_msg="离桌失败"
		}}
    end
    local res
    if self.desk.leaveDeskCallback then 
    	res = self.desk:leaveDeskCallback(user_id)
    end	
	if res and next(res) then 
		return res
	end
	--站起,删除
	self.user_manager:removeUserInfo(user_id)
	return {leaveDeskRes={
		error_code=0,
		error_msg="离桌成功"
	}}
end

--掉线
function DeskMessage:disconnect(msg_data)
	print("__DeskMessage____disconnect___", msg_data)
    local user_id = msg_data.user_id    
    local user_info = self.user_manager:getUserInfo(user_id)
    if not user_info then --玩家不在这个桌子里
    	log.error("_____调用disconnect，但玩家不在这个桌子里________",msg_data)
    end	
    local res
    if self.desk.disconnectCallback then 
    	res = self.desk:disconnectCallback(user_id)
    end		
	if res and next(res) then 
		return res
	end
	--旁观者掉线马上踢出大厅
	--从user_manager中删除
	self.user_manager:removeUserInfo(user_id)
	return {error_code=0}
end

-- 得到桌子状态
function DeskMessage:deskStatusReq(msg_data)
    local user_id = msg_data.user_id    
    local user_info = self.user_manager:getUserInfo(user_id)
    if not user_info then --玩家不在这个桌子里
    	log.error("_____调用deskStatusReq，但玩家不在这个桌子里________",msg_data)
		return {deskStatusRes={
			error_code=1,
			error_msg="玩家不在这个桌子里"
		}}
    end
    local res
    if self.desk.deskStatusCallback then 
    	res = self.desk:deskStatusCallback(user_id)
    end		
	--桌子上人数各种共用信息
	if res and next(res) then 
		return res
	end
	return {deskStatusRes={
		error_code = 1,
		error_msg = "取桌子状态错误",
	}}	

end

-- 请求坐下
function DeskMessage:sitDownReq(msg_data)
    local user_id = msg_data.user_id    
    local user_info = self.user_manager:getUserInfo(user_id)
    if not user_info then --玩家不在这个桌子里
    	log.error("_____调用sitDownReq，但玩家不在这个桌子里________",msg_data)
		return {deskStatusRes={
			error_code=1,
			error_msg="玩家不在这个桌子里"
		}}
    end
	local seat_id = self.user_manager:sitDown(user_id)
	if seat_id then 
		if self.desk.sitDownCallback then --让各个游戏做它要做的事
			self.desk:sitDownCallback()
		end
		return {sitDownRes={
			error_code = 0,
			seat_id = seat_id,
		}}
	end
	return {sitDownRes={
		error_code = 1,
		error_msg = "坐下错误",
	}}	

end
-- 得到桌子状态
function DeskMessage:standUpReq(msg_data)
    local user_id = msg_data.user_id    
    local user_info = self.user_manager:getUserInfo(user_id)
    if not user_info then --玩家不在这个桌子里
    	log.error("_____调用standUpReq，但玩家不在这个桌子里________",msg_data)
		return {deskStatusRes={
			error_code=1,
			error_msg="玩家不在这个桌子里"
		}}
    end
	local seat_id = self.user_manager:standUp(user_id)
	if seat_id then 
		if self.desk.standUpCallback then --让各个游戏做它要做的事
			self.desk:standUpCallback()
		end
		return {standUpRes={
			error_code = 0,
			seat_id = seat_id,
		}}
	end
	return {sitDownRes={
		error_code = 1,
		error_msg = "站起错误",
	}}	

end











--GM命令控制
function DeskMessage:cotrolReq(msg_data)
	-- self.machine:setGmCard(playerid,cards,repeatNum)
end

--更新属性
function DeskMessage:updatePropertyNt(playerid, prop_data)
	print("_________________玩家数据更新___",data)
	return true
end

--广播
function DeskMessage:broadcastNt( pack )
	return true
end

--安全关服
function DeskMessage:safeCloseReq(close)
	self.close = close
end
------------------------------------------------------------------------------------------------
---游戏里的操作
------------------------------------------------------------------------------------------------


return DeskMessage