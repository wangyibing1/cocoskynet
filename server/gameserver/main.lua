local skynet = require "skynet"


skynet.start(function()
	--各个服务共享的配置
	skynet.uniqueservice("confcenter")
	
	local manager = skynet.newservice("manager_service")
	skynet.call(manager, "lua", "start")

end)

