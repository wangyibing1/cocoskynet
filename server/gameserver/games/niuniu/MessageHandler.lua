--
-- @Author:      name
-- @DateTime:    2018-03-30 23:05:48
-- @Description: 消息的处理

local skynet = require "skynet"
local log = require "Logger"
local config = require "configquery"


local MessageHandler = class("MessageHandler")

---------------------------------------------------------
-- Private
---------------------------------------------------------
function MessageHandler:ctor(message_dispatch)
	self.message_dispatch = message_dispatch
	self.node_message = nil
	self.desk = nil


	
	self:register()
		
end

--注册本服务里的消息
function MessageHandler:register()

	self.message_dispatch:registerSelf("desk_start",handler(self,self.deskStart))

	-- self.message_dispatch:registerSelf('update_prop',handler(self,self.updateProperty))
	-- self.message_dispatch:registerSelf('broadcast',handler(self,self.broadcast))


	-- self.message_dispatch:registerSelf('niuniu_bank_req',handler(self,self.bank))
	-- self.message_dispatch:registerSelf('niuniu_bet_req',handler(self,self.bet))
	-- self.message_dispatch:registerSelf('niuniu_comb_req',handler(self,self.comb))
	-- self.message_dispatch:registerSelf('niuniu_changecard_req',handler(self,self.changeCard))


	self.message_dispatch:registerSelf('nnBetReq',handler(self,self.nnBetReq))
	self.message_dispatch:registerSelf('nnBankReq',handler(self,self.nnBankReq))
	self.message_dispatch:registerSelf('nnOpenCardReq',handler(self,self.nnOpenCardReq))


end


---------------------------------------------------------
-- CMD
---------------------------------------------------------
--初始化
function MessageHandler:deskStart()
	print("___deskStart______")
	self.node_message = global.node_message
	self.desk = global.desk
	self.room_config = global.room_config
	self.desk_config = global.desk_config
	self.desk_id = self.desk_config.desk_id
	self.user_manager = global.user_manager


	self.desk:start()
end




--抢庄
function MessageHandler:nnBankReq(msg_data)
	local user_id = msg_data.user_id
	local user = self.user_manager:getUser(user_id)
	if not user then 
		print("______nnBankReq_玩家不在这个游戏中_______")
		return false
	end
	local b = self.desk:setBanker(user_id, msg_data.rate)
	if not b then 
		return {nnBankRes={
			error_code=1,
			error_msg="抢庄错误",
		}}
	end
	return {nnBankRes={
		error_code=0,
	}}
end

--下注
function MessageHandler:nnBetReq(msg_data)
	local user_id = msg_data.user_id
	local user = self.user_manager:getUser(user_id)
	if not user then 
		print("______nnBetReq_玩家不在这个游戏中_______")
		return false
	end
	local b = self.desk:userBet(user_id,msg_data.rate)
	if not b then 
		return {nnBetRes={
			error_code=1,
			error_msg="下注错误",
		}}
	end
	return {nnBetRes={
		error_code=0,
	}}
end

--亮牌
function MessageHandler:nnOpenCardReq(msg_data)
	local user_id = msg_data.user_id
	local user = self.user_manager:getUser(user_id)
	if not user then 
		print("______nnBetReq_玩家不在这个游戏中_______")
		return false
	end
	local b = self.desk:userOpenCard(user_id)
	if not b then 
		return {nnOpenCardRes={
			error_code=1,
			error_msg="亮牌错误",
		}}
	end	
	return {nnOpenCardRes={
		error_code=0,
	}}	
end

--换牌
function MessageHandler:changeCard(msg_data)
	local user_id = msg_data.user_id
	local user = self.desk:getUser(user_id)	
	AssertEx(user and user.playing , 'niuniu_changecard_res', 16)

	local b,tbCard = self.desk:setuserCard(user_id)
	AssertEx(b, 'niuniu_changecard_res', 17)
	--推送玩家状态
	return {niuniu_changecard_res={card_data=tbCard}}	
end


return MessageHandler