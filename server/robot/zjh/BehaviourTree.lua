-- world100
-- 简单的行为树，节点的解析
-- https://blog.csdn.net/iamagoodguy_/article/details/39757083

require "luaext"

local BehaviourTree = class("BehaviourTree")


function BehaviourTree:ctor(bt, object)
	self.obj = object
	self.first_children = bt.nodes[1].children[1]
end

function BehaviourTree:run()
	-- print("_first_children__",first_children)
	self[self.first_children.type](self, self.first_children)
end




--------------------- 
--------------Composite Node
-- 1、Selector：实现子节点或关系，按定义顺序遍历子节点，直到有一个子节点返回true时停止，并返回true。如果子节点全部返回false，则返回false。
-- 2、RandomSelector：类似Selector，不同之处在于，按照随机顺序遍历子节点。
-- 3、Sequence：实现子节点与关系，按定义顺序遍历子节点，直到有一个节点返回false时停止，并返回false。如果子节点全部返回true，则返回true。
-- 4、Parallel：实现逗号表达式的效果，依次执行所有子节点，返回最后一个子节点的返回值。

--[[
1、Selector：实现子节点或关系，按定义顺序遍历子节点，
直到有一个子节点返回true时停止，并返回true。如果子节点全部返回false，则返回false。
--]]
function BehaviourTree:Selector(node)
	local value = false
	for _, child in ipairs(node.children) do
		if self[child.type](self, child) == true then
			value = true
			break
		end
	end
	return value
end

--[[
Sequence
实现子节点与关系，按定义顺序遍历子节点，
直到有一个节点返回false时停止，并返回false。如果子节点全部返回true，则返回true。
--]] 
function BehaviourTree:Sequence(node)
	local value = true
	if node.func then 
		self.obj[node.func](self.obj)
	end
	for _, child in ipairs(node.children) do
		local res = self[child.type](self, child)
		if res == false then
			value = false
			break
		end
	end
	return value
end

---------Behaviour Node
-- 1、Action：执行其中定义的行为，并返回true。
-- 2、ConditionAction：如果条件为真，则执行Action，并返回true；否则，不执行Action，并返回false。
--[[
1、Action：执行其中定义的行为，并返回
--]]
function BehaviourTree:Action(node)
	return self.obj[node.func](self.obj)
end
-- Condition Action
function BehaviourTree:Condition(node)
	if self.obj[node.name](self.obj) then
		self.obj[node.func](self.obj)
		return true
	else
		return false
	end
end

-------------------Condition Node
--[[
1、Filter：如果Filter为真，则执行子节点，并返回true；否则，不执行子节点，返回false。
--]]
function BehaviourTree:Filter(node)
	if self.obj[node.func](self.obj) then
		local first_children = node.children[1]
		self[first_children.type](self, first_children)
		return true
	else
		return false
	end
end

--Decorator Node
-- 1、Successor：拥有一个子节点，执行子节点后返回true。
-- 2、Failure：拥有一个子节点，执行子节点后返回false。
-- 3、Negate：拥有一个子节点，返回子节点返回值的相反之（true变false，false变true）。

return BehaviourTree