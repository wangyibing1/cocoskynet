require "luaext"
local BT = require("BehaviourTree")
local Rule = require('GameRule')
local bt_data = require('bt_data')



local Ai = class("Ai")

function Ai:ctor(robot_config)
	self.rule = Rule.new()
	self.seatid = 1 --自己的座位id
	self.my_cards = {}
	self.probability = {} --概率表
	self.bt = BT.new(bt_data, self)
	self.card_type = nil
	self.res = nil --要输还是赢
	self.oper_proba = nil --操作概率
	self.cur_proba = 0 --当前累积的
	self.cur_round = 0 --当前轮数
	self.oper_id = 0 --操作类型id 1跟注，2加注，3比牌， 4弃牌
	self.can_compare_card = false  --是否能比牌操作

	self.probability = robot_config or { {
		id = 1,
		card_type = '对子',
		res = '赢',
		round = {0,5,},
		follow = 70,
		add = 15,
		compare = 15,
		look = 5,
		giveup = 15,
	},
	{
		id = 4,
		card_type = '对子',
		res = '输',
		round = {1,10,},
		follow = 65,
		add = 10,
		compare = 65,
		look = 45,
		giveup = 5,
	}
}
	
end

--取概率
function Ai:getProbality()
	for k,v in pairs(self.probability) do 		
		if self.card_type==v.card_type and self.res==v.res then 	
			print("___v.round__",v.round, self.cur_round)
			if self.cur_round>=v.round[1] and self.cur_round<=v.round[2] then 		
				self.oper_proba = v
				print("___v",self.oper_proba)
			end
		end
	end
end

--设置牌
function Ai:setCard(cards, seat_id)

	self.cards = cards --玩家所有的牌
	self.seat_id = seat_id
	local other_cards = {}
	for k, v in pairs(cards) do 
		if v.seat_id == seat_id then 
			self.my_cards = v.cards
		else
			table.insert(other_cards, v.cards)
		end
	end
	--我的牌是不是最大
	local is_max = true
	for k,v in pairs(other_cards) do 
		if self.rule:compareCard(self.my_cards, v) == 1 then 
			is_max = false
			break
		end
	end
	if is_max then 
		self.res = "赢"
	else
		self.res = "输"
	end
	print("_____is_max__",is_max)
end


--传入所有牌
function Ai:run(round, can_compare)
	--根据牌算出赢输取到操作概率表
	self.cur_round = round or 0
	self.can_compare_card = can_compare
	self.bt:run()
	return self.oper_id
end

------------------------------行为树函数
-- 判断自己是什么牌型
function Ai:operByCardType( ... )
	local card_type = {
	    "散牌",
	    "对子",
	    "顺子",
	    "金花",
	    "顺金",
	    "豹子",	    
	}	
	local num = self.rule:getCardType(self.my_cards) 
	if num>6 or num<1 then 
		num = 1
	end
	self.card_type = card_type[num]

	print("我的牌型:",self.card_type)

	return true
end

function Ai:isWinOrLose( ... )
	print("__我_是输还是赢___",self.res)
	self:getProbality()
	self.oper_num = math.random(1,100)
	return false
end

-- 跟注
function Ai:follow( ... )
	-- print("___oper_proba___",self.oper_num, self.oper_proba)
	if self.oper_num < self.cur_proba+self.oper_proba.follow then 
		self.oper_id = 1
		print("___要跟注___")
		return false
	end
	self.cur_proba = self.cur_proba + self.oper_proba.follow	
	return true
end

-- 加注
function Ai:add( ... )
	if self.cur_round == 1 then --第一轮不让加注
		return true
	end
	if self.oper_num < self.cur_proba+self.oper_proba.add then 
		self.oper_id = 2
		print("___加注")
		return false
	end	
	self.cur_proba = self.cur_proba + self.oper_proba.add
	return true
end

-- 比牌
function Ai:compare( ... )
	if not self.can_compare_card then 
		return true
	end
	if self.oper_num < self.cur_proba+self.oper_proba.compare then 	
		self.oper_id = 3
		print("___比牌")
		return false
	end	
	self.cur_proba = self.cur_proba + self.oper_proba.compare
	return true	
end

-- 弃牌
function Ai:giveup( ... )
	if self.oper_num < self.cur_proba+self.oper_proba.giveup then 
		self.oper_id = 4
		print("___弃牌")
		return false
	end	
	self.cur_proba = self.cur_proba + self.oper_proba.giveup
	return true
end

--别的操作都没命中
function Ai:other( ... )
	self.oper_id = 1
	return true	
end

------------------public
--是否能看牌
function Ai:canLookCard()
	if not self.oper_proba then
		return false
	end
	local num = math.random(1,100)
	print("_____看牌___",num , self.oper_proba.look)
	if num < self.oper_proba.look then 
		return true
	end
	return false
end



-- function Ai:test()
-- 	local str = self.rule:printCard(self.my_cards)
-- 	print(str)
-- 	local card_type = self.rule:getCardType(self.my_cards)
-- 	print(card_type)
-- 	local card_name = self.rule:getCardTypeName(card_type)
-- 	print(card_name)

-- end

local ai = Ai.new()
local cards={
	{seat_id=1,cards={0x03,0x13,0x07,},},
	{seat_id=2,cards={0x02,0x12,0x13,},},
}
ai:setCard(cards,2)
-- ai:test()

local oper_id = ai:run(1, false)
print("___oper_id__",oper_id)


return Ai