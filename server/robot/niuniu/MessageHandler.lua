--
-- @Author:      name
-- @DateTime:    2018-08-9 23:05:48
-- @Description: 消息处理

local skynet = require "skynet"
local log = require "Logger"
local Network = require "Network"
local config = require "configquery"
local Timer = require "timer"
local FSM = require "fsm"
local HallHandler = require "HallHandler"
local GameHandler = require "GameHandler"

local MessageHandler = class("MessageHandler")

---------------------------------------------------------
-- Private
---------------------------------------------------------
function MessageHandler:ctor(message_dispatch)
	
	self.message_dispatch = message_dispatch	
	self.user_sockets = {} --用户连接的基本信息
	self.node_message = node_message
	local ip = "127.0.0.1"
	local port = 12101
	self.network = Network.new(ip,port)
	self.cmd = nil
	self.user_id = nil
	self.is_banker = false --是庄
	self.character = nil --行为配置
	self.rob_num_list = {} --可用抢庄倍数
	self.bet_num_list = {} --可用下注倍数
	self.first_enter = true --第一次进入桌子，不随机换桌

	math.randomseed()
	self.timer = Timer:new()
	self.max_wait_time = 30
	self.hallHandler = nil
	self.gameHandler = nil

	-- 注册消息
	self:register()

	--休息，开始， 抢庄，下注，组牌，结算
	--state1 --eventName --state2 --callback
	self.game_state_transition = {
		{"rest", 		"to_start", 	"start", 		function() self:onStart() end },
		{"start", 		"to_bank", 		"bank", 		function() self:onBank() end },
		{"bank", 		"to_bankok", 	"bankok", 		function() self:onBankok() end },
		{"bankok", 		"to_bet",		"bet", 			function() self:onBet() end },
		{"bet", 		"to_open",		"open", 		function() self:onOpen() end },
		{"open", 		"to_settlement","settlement", 	function() self:onSettlement() end },
		{"settlement", 	"to_rest", 		"rest", 		function() self:onRest() end},
		{"rest", 		"to_wait", 		"wait", 		function() self:onWait() end},		
		{"wait", 		"to_rest", 		"rest", 		function() self:onRest() end},			
	}
	-- 状态机
	self.fsm = FSM.new(self.game_state_transition) 
	self.fsm:set("rest")
	
	-- 定时器，退出游戏
	self:checkStatus(true)
end

--注册本服务里的消息
function MessageHandler:register()

	self.message_dispatch:register('init',handler(self,self.init))

	-- 场景消息
	self:registerGameMessage( "MSG_G2C_SceneGameIdle", handler(self,self.sceneGameIdlezNt))
	self:registerGameMessage( "MSG_G2C_SceneRobBanker", handler(self,self.sceneRobBankerNt))
	self:registerGameMessage( "MSG_G2C_SceneBetJetton", handler(self,self.sceneBetNt))
	self:registerGameMessage( "MSG_G2C_SceneBrightCard", handler(self,self.sceneBrightCardNt))

	-- 抢庄
	self:registerGameMessage( "MSG_G2C_DealCard", handler(self,self.dealCardNt))
	self:registerGameMessage( "MSG_G2C_StartRobBanker", handler(self,self.robBankerNt))
	self:registerGameMessage( "MSG_G2C_RobBankerNotify", handler(self,self.userRobBankerNt))
	self:registerGameMessage( "MSG_G2C_RobBankerResult", handler(self,self.robBankerRes))
	self:registerGameMessage( "MSG_G2C_ChooseBanker", handler(self,self.chooseBankerNt))

    -- //下注
	self:registerGameMessage( "MSG_G2C_StartBetJetton", handler(self,self.startBetNt))
	self:registerGameMessage( "MSG_G2C_BetJettonNotify", handler(self,self.userBetNt))
	self:registerGameMessage( "MSG_G2C_BetJettonResult", handler(self,self.userBetRes))

    -- //亮牌
	self:registerGameMessage( "MSG_G2C_StartBrightCard", handler(self,self.startBrightCardNt))
	self:registerGameMessage( "MSG_G2C_BrightCardNotify", handler(self,self.brightCardNt))
	self:registerGameMessage( "MSG_G2C_BrightCardResult", handler(self,self.brightCardRes))

    -- //结算
	self:registerGameMessage( "MSG_G2C_GameSettlement", handler(self,self.settlementNt))


end

-- 初始化玩家信息
function MessageHandler:init(pbc_env, user_info)
	print("________user___init__________")
	self.cmd = config.message_cmd
	-- print("__config.robot_niuniu_set___",config.robot_niuniu_set)
	local robot_character_set = config.robot_niuniu_set.character
	local character_type = math.random(1,3) --性格类型
	self.character = robot_character_set[character_type]
	self.user_info = user_info
	
	-- print("___self.character__",self.character)
	self.network:init(pbc_env)
	self.hallHandler = HallHandler.new(self.message_dispatch, self.network, user_info)
	self.gameHandler = GameHandler.new(self.message_dispatch, self.network, user_info, self)
end

function MessageHandler:registerByName(package_name, message_name, callback)
	if not callback then 
		callback = handler(self,self[message_name])
	end
	self.network:register(package_name..message_name, callback)
end

function MessageHandler:registerGameMessage(message_name, callback)
	local package_name = "proto.msg.game_niuniu."
	self:registerByName(package_name,  message_name, callback)
end

function MessageHandler:exit()
	self.network:close()
	skynet.exit()
end

function MessageHandler:print(...)
	-- if self.user_id == 39 then 
		print(...)
	-- end
end
function MessageHandler:checkError(data)
	if data.error_code and data.error_code > 0 then 
		self:exit()
	end
end

function MessageHandler:checkStatus(check)
	-- 长时间处于等待状态的机器人，自动退出
	if check then 
		self.timer_check_status = self.timer:register(self.max_wait_time, nil, handler(self, self.exit))
	elseif self.timer_check_status then 
		self.timer:unregister(self.timer_check_status)
	end
end

-- 暂停主协程
function MessageHandler:sleep(times)
	local time = math.random(times[1],times[2]) * 100
	skynet.sleep(time)
end

-- 取下注值
function MessageHandler:getBetNum()
	local bets = self.character.bet_num
	local num = math.random(bets[1],bets[2])
	num = self.bet_num_list[num]
	return num or 1
end

-- 取抢庄倍数
function MessageHandler:getRobBankerNum()
	local bets = self.character.rob_banker_num
	local num = math.random(bets[1],bets[2])
	num = self.rob_num_list[num]
	return num or 1	
end

--换桌
function MessageHandler:changeTable()
	local nums = self.character.change_desk
	local num = math.random(1,100)
	if num >= nums[1] and num <= nums[2] then 
		self:exit()
		return true
	end	
	return false
end
---------------------------------------------------------------
--req
---------------------------------------------------------------
--用户准备
function MessageHandler:userReadyReq(data)
	if not self.first_enter then 
		local res = self:changeTable() --可能换桌	
		if res then return end
	end	
	local times = self.character.ready_speed
	self:sleep(times)
	-- print("________ready_over_",os.time() - time)
	local body = {
		extend_data = {1},
	}
	self.network:send(self.cmd.DMSG_MDM_GAME,self.cmd.DMSG_C2G_UserReady,body)
end
--下注请求
function MessageHandler:betJettonReq(data)
	if self.is_banker then --庄不用下注
		return 
	end
	local body = {
		seat_id = self.user_info.seat_id,
		bet_jetton = self:getBetNum(),
	}
	-- print("___________self.cmd.DMSG_C2G_BetJetton______")
	self:sleep(self.character.bet_speed)	
	self.network:send(self.cmd.DMSG_MDM_GAME_NIUNIU,self.cmd.DMSG_C2G_BetJetton,body)    	
end
--亮牌请求
function MessageHandler:brightCardReq(data)
	local body = {
		seat_id = self.user_info.seat_id,		
	}
	self:sleep(self.character.show_card_speed)	
	self.network:send(self.cmd.DMSG_MDM_GAME_NIUNIU,self.cmd.DMSG_C2G_BrightCard,body) 	
end
--玩家抢庄请求
function MessageHandler:robBankerReq(data)
	-- self:print("__玩家抢庄___MSG_C2G_RobBanker_____",data)
    -- required uint32 seat_id         = 1;    //座位标识
    -- required uint32 multiple        = 1;    //抢庄倍数
	local body = {
		seat_id = self.user_info.seat_id,
		multiple = self:getRobBankerNum(),
	}
	self:sleep(self.character.rob_banker_speed)
	self.network:send(self.cmd.DMSG_MDM_GAME_NIUNIU,self.cmd.DMSG_C2G_RobBanker,body)    
end

---------------------------------------------------------------
--res
---------------------------------------------------------------
-- 抢庄结果
function MessageHandler:robBankerRes(data)
	self:print("__抢庄结果___MSG_G2C_RobBankerResult_____",data)
end
--下注结果
function MessageHandler:userBetRes(data)
	self:print("__下注结果_____",data)
end
--亮牌结果
function MessageHandler:brightCardRes(data)
	self:print("__亮牌结果_____",data)
end

---------------------------------------------------------------
--nt
---------------------------------------------------------------
function MessageHandler:sceneGameIdlezNt(data)
	self:print("__空场景___MG_G2C_SceneGameIdle_____",data)
	self.rob_num_list = data.config.multiples --可用抢庄倍数
	self.bet_num_list = data.config.jettons --可用下注倍数
	if not self.first_enter then 
		self:changeTable() --可能换桌	
	end
	self.first_enter = false

	self:userReadyReq() --准备
end
function MessageHandler:sceneRobBankerNt(data)
	self:print("__抢庄场景___MSG_G2C_SceneRobBanker_____",data)
	self.rob_num_list = data.config.multiples --可用抢庄倍数
	self.bet_num_list = data.config.jettons --可用下注倍数	
end
function MessageHandler:sceneBetNt(data)
	self:print("__下注场景___MSG_G2C_SceneBetJetton_____",data)
	self.rob_num_list = data.config.multiples --可用抢庄倍数
	self.bet_num_list = data.config.jettons --可用下注倍数	
end
function MessageHandler:sceneBrightCardNt(data)
	self:print("__亮牌场景___MSG_G2C_SceneBrightCard_____",data)
	self.rob_num_list = data.config.multiples --可用抢庄倍数
	self.bet_num_list = data.config.jettons --可用下注倍数	
end

--发牌
function MessageHandler:dealCardNt(data)
	self:print("__发牌___MG_G2C_SceneGameIdle_____",data)		
	self.is_banker = false
	self:checkStatus(false)
end

-- 开始抢庄
function MessageHandler:robBankerNt(data)
	self:print("__开始抢庄___MSG_G2C_StartRobBanker_____",data)

	self:robBankerReq()
end

function MessageHandler:userRobBankerNt(data)
	self:print("__抢庄通知___MSG_G2C_RobBankerNotify_____",data)
   
end

function MessageHandler:chooseBankerNt(data)
	self:print("__庄家确定___MSG_G2C_ChooseBanker_____",data)
    if self.user_info.seat_id == data.banker_seat_id then 
    	self.is_banker = true
    end
end

--开始下注
function MessageHandler:startBetNt(data)
	self:print("__开始下注通知_____",data)
	self:betJettonReq()
end

--玩家下注通知
function MessageHandler:userBetNt(data)
	self:print("__玩家下注通知_____",data)
end

--开始亮牌
function MessageHandler:startBrightCardNt(data)
	self:print("__开始亮牌通知_____",data)
	self:brightCardReq()
end
--亮牌通知
function MessageHandler:brightCardNt(data)
	self:print("__玩家亮牌通知_____",data)
end

--结算
function MessageHandler:settlementNt(data)
	self:print("__结算结果_____",data)

	self:checkStatus(true)
end
  


return MessageHandler