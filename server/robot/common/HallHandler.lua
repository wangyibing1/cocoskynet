-- 大厅逻辑
local skynet = require "skynet"
local log = require "Logger"
local config = require "configquery"



local HallHandler = class("HallHandler")

---------------------------------------------------------
-- Private
---------------------------------------------------------
function HallHandler:ctor(message_dispatch, network, user_info)
	
	self.message_dispatch = message_dispatch	
	self.node_message = node_message
	self.network = network
	self.user_info = user_info -- 登录需要的用户信息

	local ip = "127.0.0.1"
	-- local ip = "192.168.2.21"
	local port = 12101
	self.cmd = nil
	self.user_id = nil
	self.game_user = nil
	self.desk_id = nil
	self.seat_id = nil
	-- self.timer = Timer:new()
	self.cmd = config.message_cmd

	self:register()

	--登录
	skynet.fork(function()
		skynet.sleep(200)
		self.network:open()
		self:userLoginReq()
	end)

end

--注册本服务里的消息
function HallHandler:register()

	self:registerLoginMessage("MSG_L2C_UserLoginResult", handler(self, self.loginRes))
	self:registerLoginMessage("MSG_L2C_UserRegisterResult", handler(self, self.registerRes))
	self:registerLoginMessage("MSG_L2C_UserLogoutResult", handler(self, self.logoutRes))
	self:registerLoginMessage("MSG_L2C_UserLoginAuthResult", handler(self, self.loginAuthRes))

	self:registerHallMessage("MSG_H2C_GameCategoryList", handler(self, self.gameCategoryListNt))
	self:registerHallMessage("MSG_H2C_GameInfoList", handler(self, self.gameInfoListNt))
	self:registerHallMessage("MSG_H2C_EnterHallResult", handler(self, self.enterHallRes))
	self:registerHallMessage("MSG_H2C_EnterGameResult", handler(self, self.enterGameRes))
	self:registerHallMessage("MSG_H2C_GameClassifyList", handler(self, self.gameClassifyListNt))

end

function HallHandler:registerByName(package_name, message_name, callback)
	if not callback then 
		callback = handler(self,self[message_name])
	end
	self.network:register(package_name..message_name, callback)
end
function HallHandler:registerHallMessage(message_name, callback)
	local package_name = "proto.msg.hall."
	self:registerByName(package_name,  message_name, callback)
end
function HallHandler:registerLoginMessage(message_name, callback)
	local package_name = "proto.msg.login."
	self:registerByName(package_name,  message_name, callback)
end
function HallHandler:registerGameMessage(message_name, callback)
	local package_name = "proto.msg.game_niuniu."
	self:registerByName(package_name,  message_name, callback)
end

function HallHandler:exit()
	self.network:close()
	skynet.exit()
end

function HallHandler:print(...)
	-- if self.user_id == 39 then 
		print(...)
	-- end
end
function HallHandler:checkError(data)
	if data.error_code and data.error_code > 0 then 
		self:exit()
	end
end

---------------------------------------------------------------
--req
---------------------------------------------------------------
--用户注册
function HallHandler:registerReq()
	local data = {
		-- version
		-- mac_addr
		-- package_name
		domain_id = 1,
		device_type = 'android',
		channel_id = 1,
		-- login_type
		account = "test19",--..math.random(1,100),
		password = "123456",
		nickname = "test",			
		face_id = 0,
		-- invite_code = "123",
		-- reference_id
		-- site_code
		-- nation_code
		-- city_code
		sex = 1,			
	}
	self.network:send(self.cmd.DMSG_MDM_LOGIN,self.cmd.DMSG_C2L_UserRegister,data)
end
--用户登陆
function HallHandler:userLoginReq()
-- //用户登陆
-- message MSG_UserLogin
-- {
-- 	required string		version			= 1;	// 版本号
-- 	required string		mac_addr		= 2;	// MAC物理地址(UUID)
-- 	optional uint32		device_type		= 3;	// 手机设备
-- 	required uint32		channel_id		= 4;	// 渠道标识
-- 	required uint32		login_type  	= 5;	// 登录类型
-- 	optional string		accounts 		= 6;	// 登录帐号
-- 	optional string		password 		= 7;	// 登录密码
-- 	optional uint32		user_id			= 8;	// ID登录方式
-- 	optional string		token			= 9;	// 登录token
-- }

	local data = {
		version = "1.0",
		mac_addr = "123",
		device_type = 1,
		channel_id = 1,
		login_type = 2,
		account = self.user_info.robot_name,
		password = self.user_info.robot_password,
		user_id = 1,
		token = '',
		mac_addr = "12324314",
	}
	self.network:send(self.cmd.DMSG_MDM_LOGIN,self.cmd.DMSG_C2L_UserLogin,data)
end


--登陆token认证
function HallHandler:loginAuthReq(data)
	-- local body = {
	-- 	user_id = 39,
	-- 	token = 'MTU1NjAwNzE0OQ==',
	-- }
	-- self:print("_____data__",data)
	self.network:send(self.cmd.DMSG_MDM_LOGIN,self.cmd.DMSG_C2L_UserLoginAuth,data)
end

--请求进入大厅
function HallHandler:enterHallReq()
	local body = {
		user_id = self.user_id,
	}
	self.network:send(self.cmd.DMSG_MDM_HALL,self.cmd.DMSG_C2H_EnterHall,body)
end

-- 请求取桌子id
function HallHandler:enterGameReq()
	local body = {
		game_id = self.user_info.game_id,
		group_id = self.user_info.group_id,
		server_id = self.user_info.server_id,
		desk_id = self.user_info.desk_id,
	}
	self.network:send(self.cmd.DMSG_MDM_HALL,self.cmd.DMSG_C2H_EnterGameEx,body)	
end

-- 请求进入桌子
function HallHandler:enterDeskReq(data)
	local body = {
		desk_id = data.desk_id,
	}
	self.user_info.desk_id = data.desk_id
	self.network:send(self.cmd.DMSG_MDM_GAME,self.cmd.DMSG_C2G_EnterDesk,body)
end



---------------------------------------------------------------
--res
---------------------------------------------------------------
-- 登录服务器登录结果
function HallHandler:loginRes(data)
	print("___登录服务器登录结果____",data)
	if not data.error_code or data.error_code == 0 then 
		print("___登录服器成功通过___")
		--切换登录网关
		--用token去验证登录
		self.network:close()
		self.user_info.user_id =  data.user_info.user_id
		local addr = data.server_addr
		local ip,port = string.match(addr,"^([^:]*):(.*)$")
		skynet.fork(function()
			skynet.sleep(100)	
			self.network:open(ip,port)
			local body = {
				user_id = data.user_info.user_id,
				token = data.token,
			}		
			self:loginAuthReq(body)
		end)
	end	
end

-- 注册结果
function HallHandler:registerRes(data)
	if data.error_code and data.error_code > 0 then 
		self:print("______",data.error_msg)
		return
	end
	self:print("___注册成功____",data)
end

-- 登出返回
function HallHandler:logoutRes(data)
	self:print("____登出返回____________",data)
end

-- token登录返回
function HallHandler:loginAuthRes(data)
	--token认证
	self:print("___token认证————",data)
	if data.error_code == 0 then 
		self:enterHallReq()
	end
end

-- 进入大厅结果
function HallHandler:enterHallRes(data)
	self:print("____进入大厅返回",data)
end

-- 大厅推送的游戏种类
function HallHandler:gameCategoryListNt(data)
	self:print("___大厅推送游戏种类成功____",data)
end

-- 大厅推送的游戏列表
function HallHandler:gameInfoListNt(data)
	self:print("___大厅推送游戏列表成功____")
	-- --登出
	-- local data = {
	-- 	user_id = self.user_id,
	-- }
	-- self.network:send(self.cmd.DMSG_MDM_LOGIN, self.cmd.DMSG_C2L_UserLogout,data)
	-- 进入指定游戏
	self:enterGameReq()	
end

--游戏列表
function HallHandler:gameClassifyListNt(data)
	self:print("___大厅推送游戏列表成功_11___")
end


-- 返回桌子id
function HallHandler:enterGameRes(data)
	self:print("____取游戏桌子id返回____________",data)
	self:checkError(data)

	self:enterDeskReq(data)
end

return HallHandler