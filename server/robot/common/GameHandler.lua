-- 游戏进桌逻辑
local skynet = require "skynet"
local log = require "Logger"
local config = require "configquery"



local GameHandler = class("GameHandler")

---------------------------------------------------------
-- Private
---------------------------------------------------------
function GameHandler:ctor(message_dispatch, network, user_info, game)
	
	self.message_dispatch = message_dispatch	
	self.node_message = node_message
	self.network = network
	self.user_info = user_info -- 登录需要的用户信息

	self.cmd = nil
	self.game = game
	self.cmd = config.message_cmd

	self:register()

end

--注册本服务里的消息
function GameHandler:register()
	
	self:registerGameMessage("MSG_G2C_EnterDeskResult", handler(self, self.enterDeskRes))
	self:registerGameMessage("MSG_G2C_LeaveDeskResult", handler(self, self.leaveDeskRes))
	self:registerGameMessage("MSG_G2C_SitDownResult", handler(self, self.sitdownRes))
	self:registerGameMessage("MSG_G2C_StandUpResult", handler(self, self.standupRes))
	self:registerGameMessage("MSG_G2C_ChangeDeskResult", handler(self, self.changeDeskRes))
	self:registerGameMessage("MSG_G2C_UserReadyResult", handler(self, self.readyRes))
	self:registerGameMessage("MSG_G2C_GameConfig", handler(self, self.gameConfigNt))
	self:registerGameMessage("MSG_G2C_UserInfo", handler(self, self.userInfoRes))
	self:registerGameMessage("MSG_G2C_EnterFinish", handler(self, self.enterFinishNt))
	self:registerGameMessage("MSG_G2C_UserStatusInfo", handler(self, self.userStatusInfoNt))
	self:registerGameMessage("MSG_G2C_ClientReadyResult", handler(self, self.clientReadyRes))
	self:registerGameMessage("MSG_G2C_GameStartNotify", handler(self, self.gameStartNt))
	self:registerGameMessage("MSG_G2C_GameStatus", handler(self, self.gameStatusNt))
	self:registerGameMessage("MSG_G2C_LookonConfigNotify", handler(self, self.lookConfigNt))
	self:registerGameMessage("MSG_G2C_UserTrusteeshipNotify", handler(self, self.trusteeshipNt))
	self:registerGameMessage("MSG_G2C_UserTreasureInfo", handler(self, self.treasureInfoNt))
	self:registerGameMessage("MSG_G2C_UserGameInnings", handler(self, self.numOfGameNt))
	self:registerGameMessage("MSG_G2C_GameEndNotify", handler(self, self.gameEndNt))
	

end

-- 通过消息名注册监听
function GameHandler:registerByName(package_name, message_name, callback)
	if not callback then 
		callback = handler(self,self[message_name])
	end
	self.network:register(package_name..message_name, callback)
end

function GameHandler:registerGameMessage(message_name, callback)
	local package_name = "proto.msg.game."
	self:registerByName(package_name,  message_name, callback)
end

function GameHandler:exit()
	self.network:close()
	skynet.exit()
end

function GameHandler:print(...)
	-- if self.user_id == 39 then 
		print(...)
	-- end
end
function GameHandler:checkError(data)
	if data.error_code and data.error_code > 0 then 
		self:exit()
	end
end

---------------------------------------------------------------
--req
---------------------------------------------------------------
-- 离桌请求
function GameHandler:leaveDeskReq()
	local body = {
		desk_id = self.user_info.desk_id,
	}
	self.network:send(self.cmd.DMSG_MDM_GAME,self.cmd.DMSG_C2G_LeaveDesk,body)
end
-- 坐下请求
function GameHandler:sitdownReq()
	local body = {
		desk_id = self.user_info.desk_id,
		seat_id = -1,
		desk_password = nil,
	}
	-- print("____MSG_C2G_SitDown___",body)
	self.network:send(self.cmd.DMSG_MDM_GAME,self.cmd.DMSG_C2G_SitDown,body)
end
function GameHandler:standupReq()
	local body = {
		desk_id = self.user_info.desk_id,
		seat_id = self.user_info.seat_id,
		force_leave	= nil,	
	}
	self.network:send(self.cmd.DMSG_MDM_GAME,self.cmd.DMSG_C2G_StandUp,body)
end
function GameHandler:changeDeskReq()
	local body = {
		desk_id = 1,
		seat_id = 1,
	}
	self.network:send(self.cmd.DMSG_MDM_GAME,self.cmd.DMSG_C2G_ChangeDesk,body)
end

--客户端准备
function GameHandler:clientReadyReq()
	local body = {
		allow_lookon = 1,
	}
	self.network:send(self.cmd.DMSG_MDM_GAME,self.cmd.DMSG_C2G_ClientReady,body)
end


---------------------------------------------------------------
--res
---------------------------------------------------------------
function GameHandler:enterDeskRes(data)
	self:print("_进入桌子__MSG_G2C_EnterDeskResult_____",data)
	self:checkError(data)
end
function GameHandler:leaveDeskRes(data)
	self:print("_离开桌子_MSG_G2C_LeaveDeskResult______",data)
end
function GameHandler:sitdownRes(data)
	self:print("__坐下结果__MSG_C2G_SitDownResult____",data)
	self:checkError(data)
	self:clientReadyReq() --客户端准备
end
function GameHandler:standupRes(data)
	self:print("__站起_MSG_G2C_StandUpResult_____",data)
end
function GameHandler:changeDeskRes(data)
	self:print("__换桌__MSG_G2C_ChangeDeskResult____",data)
end
function GameHandler:readyRes(data)
	self:print("__准备_MSG_G2C_UserReadyResult_____",data)
end
function GameHandler:gameConfigNt(data)
	self:print("__游戏配置_MSG_G2C_GameConfig_____",data)

end
function GameHandler:userInfoRes(data)
	self:print("__用户信息_MSG_G2C_UserInfo_____",data, self.user_info)
	if self.user_info.user_id == data.user_id then 
		self.user_info.user_status = data.user_status
	end
end

function GameHandler:enterFinishNt(data)
	self:print("__进入结束_MSG_G2C_EnterFinish_____",data)
	self:print("______game_user__",self.user_info)
	if not self.user_info.seat_id or self.user_info.seat_id<=0 or self.user_info.seat_id>=6 then 
		self:sitdownReq()
	elseif self.user_info.user_status >= 4 then 
		self:clientReadyReq() --客户端准备
	end	
end


function GameHandler:userStatusInfoNt(data)
	self:print("__玩家状态_MSG_G2C_UserStatusInfo_____",data)
	if data.user_id ~= self.user_info.user_id then 
		return 
	end

	self.user_info.seat_id = data.seat_id
	-- self.game_user.desk_id = data.desk_id 
	if data.user_status == 4 then --坐下了
		-- self:clientReadyReq() --客户端准备
	elseif data.user_status == 5 then --已准备		
		-- self:MSG_C2G_StandUp()
	end
end

function GameHandler:clientReadyRes(data)
	self:print("__客户端准备结果___MSG_G2C_ClientReadyResult_____",data)


end

function GameHandler:gameStartNt(data)
	self:print("__游戏开始通知___MSG_G2C_GameStartNotify_____",data)

end

function GameHandler:gameStatusNt(data)
	self:print("__MSG_G2C_GameStatus_____",data)

end

function GameHandler:lookConfigNt(data)
	self:print("__MSG_G2C_LookonConfigNotify_____",data)

end

function GameHandler:trusteeshipNt(data)
	self:print("__MSG_G2C_UserTrusteeshipNotify_____",data)

end
function GameHandler:treasureInfoNt(data)
	self:print("__金钱变化_____",data)

end
function GameHandler:numOfGameNt(data)
	self:print("__游戏局数_____",data)

end
function GameHandler:gameEndNt(data)
	self:print("__游戏结束通知_____",data)
	self.game:userReadyReq()
end


return GameHandler