--
-- @Author:      name
-- @DateTime:    2018-03-30 23:05:48
-- @Description: 节点内消息的处理

local skynet = require "skynet"
local log = require "Logger"
local config = require "configquery"
local ProtoLoader = require "ProtoLoader"
local cluster = require "skynet.cluster"


local MessageHandler = class("MessageHandler")

---------------------------------------------------------
-- Private
---------------------------------------------------------
function MessageHandler:ctor(message_dispatch, node_message)

    self.proto_file = skynet.getenv("proto_config") --proto文件路径 
    self.server_id = tonumber(skynet.getenv("server_id")) or 0
    self.server_type = skynet.getenv("server_type") or ""
    self.server_name = skynet.getenv("server_name") or ""
    self.cluster_addr = skynet.getenv("cluster_addr")
    self.cluster_name = skynet.getenv("cluster_name")
    self.zone_cluster_id = skynet.getenv("zone_cluster_id")
    self.node_cluster_id = skynet.getenv("node_cluster_id")


	self.message_dispatch = message_dispatch	
	self.server_info = nil --此服务器的相关信息
	self.gate_info = nil --对应的网关服务器信息
			
	self.node_message = node_message
	self.proto_loader = ProtoLoader.new() --proto加载


	self:register()
	
end

--注册本服务里的消息
function MessageHandler:register()

	self.message_dispatch:registerSelf('start',handler(self,self.start))
	self.message_dispatch:registerSelf('server_register_req', handler(self,self.onServerRegisterReq))
	self.message_dispatch:registerSelf('server_register_res', handler(self,self.serverRegisterRes))  
end

-- -- 向节点服务器注册
-- function MessageHandler:serverRegisterReq()
--     local server_info = {
--         server_id = self.server_id,
--         server_type = self.server_type,
--         server_name = self.server_name,
--         cluster_addr = self.cluster_addr,
--         cluster_name = self.cluster_name,
--     }
    
--     self.node_message:sendNodeProxy(self.node_cluster_id, "server_register_req", server_info)     
-- end

---------------------------------------------------------
-- CMD
---------------------------------------------------------
function MessageHandler:start()

	local pbc_env = self.proto_loader:init(self.proto_file, {'common','game','gate','web','zone','hall',})
	self.node_message:initProto(pbc_env)
	--skynet控制台
	-- skynet.newservice("debug_console",svr_config.debug_port)
    self.server_manager = global.server_manager
	-- self:serverRegisterReq()

end

-- --nodeserver 返回注册成功
-- function MessageHandler:serverRegisterRes(msg_data)

--     print("__serverRegisterRes___",msg_data)
--     local server_manager = self.server_manager
--     local server_info = {
--         server_id = self.server_id,
--         server_type = self.server_type,
--         server_name = self.server_name,
--         cluster_addr = self.cluster_addr,
--         cluster_id = self.cluster_id,        
--         max_client = server_manager:getMaxClient(),
--         online_count = server_manager:getOnlineCount(),
--     }   
--     self.node_message:sendNodeProxy(self.zone_cluster_id, "server_info_nt", server_info)
-- end


--gateserver注册过来
function MessageHandler:onServerRegisterReq(msg_data)
    print("___msg_data__",msg_data)
    local msg_body = msg_data
    
    local server = self.server_manager:getServer(msg_body.server_id)
    if server then
        --发送注册结果
        local result = {
            error_code = 0,
            error_msg = "服务器注册失败，有相同服务器标识的服务器已经注册了！"
        }
        self.node_message:sendNodeProxy(msg_body.cluster_id, "server_register_res", result)
        return 
    else
        server = {
            server_id = msg_body.server_id,
            server_type = msg_body.server_type,
            server_name = msg_body.server_name,
            cluster_addr = msg_body.cluster_addr,
            cluster_name = msg_body.cluster_name,          
        }

        self.server_manager:addServer(server)
    end

    --把注册的服务器添加到自己的集群列表中
    if not server.cluster_name then 
        return 
    end

    -- 重新加载集群配置
    local cluster_name = server.cluster_name
    local cluster_addr = server.cluster_addr 
    local cluster_config = {
        [cluster_name] = cluster_addr,
    }
    cluster.reload(cluster_config)
    -- print("_____收到注册_________",cluster_config,msg_body)
    -- 添加
    -- self.node_message:addServer(server.server_id, server.server_type, cluster_name)

    --发送注册结果
    local result = {
        error_code = 0,
        error_msg = "服务器注册成功"
    }
    self.node_message:sendNodeProxy(cluster_name, "server_register_res", result)

end


return MessageHandler