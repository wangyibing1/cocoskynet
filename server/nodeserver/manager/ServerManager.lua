--
-- @Author:      name
-- @DateTime:    2019-04-28 23:05:48
-- @Description: 服务器管理


local ServerManager = class("ServerManager")

--初始化
function ServerManager:ctor()
    -- local server = {
    --     server_id = 0,
    --     server_type = 0,
    --     cluster_addr = "",
    --     server_name = "",
    --     cluster_id = "",
    -- }

    -- local user = {
    --     user_id = user_id,
    --     token = token,        
    --     server_id = gate_server_id,
    --     server_type = "gateserver",
    --     server_name = "游戏网关",
    --     server_addr = gate_server_addr,
    --     server_port = gate_server_port,
    --     server_wport = gate_server_wport, 
    --     game_server_id = game_server_id,
    --     game_server_id = game_server_id,
    --     desk_id = desk_id,    
    -- }
    self.server_list = {}
    self.user_list = {}  
end

--添加服务器信息
function ServerManager:addServer(server)
    local server_id = server.server_id
    self.server_list[server_id] = server
end

--移除服务器信息
function ServerManager:removeServer(server_id)
    if self.server_list[server_id] then 
        self.server_list[server_id] = nil
        return true
    end
    return false
end

--获取服务器信息
function ServerManager:getServer(server_id)
    return self.server_list[server_id]
end

--获取服务器列表
function ServerManager:getServerList()
    return self.server_list
end

--获取服务器信息
function ServerManager:getServerListByType(server_type)    
    local server_list = {}
    for _, v in pairs(self.server_list) do         
        if v.server_type == server_type then 
            table.insert(server_list, v)
        end
    end    
    return server_list
end

function ServerManager:addUser(user)
    local user_id = user.user_id
    if not self.user_list[user_id] then 
        self.user_list[user_id] = user 
    else
        for k, v in pairs(user) do 
            self.user_list[user_id][k] = v
        end
    end    
end

function ServerManager:getGateServer(user_id)
    if not self.user_list[user_id] then 
        return 
    end 
    local user_info = self.user_list[user_id]
    if user_info.gate_server_id then 
        return "gateserver_"..user_info.gate_server_id
    end
end
function ServerManager:getHallServer(user_id)
    if not self.user_list[user_id] then 
        return 
    end 
    local user_info = self.user_list[user_id]
    if user_info.hall_server_id then 
        return "hallserver_"..user_info.hall_server_id
    end
end
function ServerManager:getGameServer(user_id)
    if not self.user_list[user_id] then 
        return 
    end 
    local user_info = self.user_list[user_id]
    if user_info.game_server_id then 
        return "gameserver_"..user_info.game_server_id, user_info.desk_id
    end
end

return ServerManager

