--
-- @Author:      name
-- @DateTime:    2018-03-30 23:05:48
-- @Description: 节点内消息的处理
require "skynet.manager"
local skynet = require "skynet"
local log = require "Logger"
local config = require "configquery"
local cluster = require "skynet.cluster"

local ClusterClient = class("ClusterClient")

---------------------------------------------------------
-- Private
---------------------------------------------------------
function ClusterClient:ctor(message_dispatch, node_message)
	self.server_type = skynet.getenv("server_type") --
	self.server_id = skynet.getenv("server_id") --服务器id
	self.cluster_name = skynet.getenv("cluster_name")
	self.message_dispatch = message_dispatch	
	self.node_message = node_message
	self.heartbeat_delay = 500 --心跳时间
	self.cluster_info = {} -- 自己的集群信息

	self:register()
	-- self:heartbeatReq()
end

--注册本服务里的消息
function ClusterClient:register()
	-- self.message_dispatch:registerSelf('client_start',handler(self, self.openCluster))

	self.message_dispatch:registerSelf('reload_cluster_nt',handler(self,self.reloadClusterNt))

end

-- 向zoneserver 发送心跳包
function ClusterClient:heartbeatReq()	
	local function func()			
		-- print("__________connect__",self.cluster_name)
		skynet.sleep(100)
		self.node_message:sendZoneProxy('cluster_connect_req', self.cluster_name)
		-- while true do 
		-- 	skynet.sleep(self.heartbeat_delay)		
		-- 	self.node_message:sendZoneProxy('cluster_heartbeat_req', self.cluster_info)				
		-- end
	end	
	skynet.fork(func)
end

---------------------------------------------------------
-- CMD
---------------------------------------------------------
function ClusterClient:openCluster()

	local cluster_addr = skynet.getenv("cluster_addr")	
	-- print("___self.cluster_id__",cluster_addr,self.cluster_id)
	--集群配置表
	self.cluster_info = {
		[self.cluster_name] = cluster_addr,
	} 
	--集群表加载
	cluster.reload(self.cluster_info)
	cluster.open(self.cluster_name)

	--skynet控制台
	-- skynet.uniqueservice("debug_console",svr_info.debug_port)
	print("#####################",self.server_type, self.server_id,"#####################")
end

-- 加载集群列表
function ClusterClient:reloadClusterNt(cluster_list)
	-- print("______reloadCluster___",skynet.getenv("svr_id"), cluster_list)
	cluster.reload(cluster_list)
	-- for k, v in pairs(cluster_list) do 
	-- 	if self.cluster_name == k then 
	-- 		self.cluster_info[k] = v
	-- 	end
	-- end
	return true
end



return ClusterClient