--
-- Author:      name
-- DateTime:    2018-04-21 16:07:56
-- Description: 语言类

--每个服务最好只用这个作为全局变量
global = global or {}

local Language = {}

function Language.new(...)
	local instance = setmetatable({}, {__index=Language})
	instance:ctor(...)
	return instance
end


function Language:ctor(name)
	local name = name or "ch"
	self.tbLan = {
		"ch",
		"en",
	}
	self.tbText = {}
	for k,v in pairs(self.tbLan) do 
		if name == v then 
			self.tbText = require(v)
			break
		end
	end
	if not next(self.tbText) then 
		self.tbText = require("ch")
	end
end

function Language:get(id)
	return self.tbText[id] or ""
end

if not global.language then 
	local language = Language.new()
	global.language = language
end

return global.language