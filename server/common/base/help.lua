	
local skynet = require "skynet"
local log = require "Logger"
local lang = require "Language"


local xpcall = xpcall
local pcall = pcall


function x_pcall(f, ...)
	--把堆栈调用返回
	return xpcall(f, debug.traceback, ...)
end

function xx_pcall(f, ...)
	local function func( ok, result, ... )
		if not ok then
			--把错误打印出来
			log.error("##消息传递错误:", result, ...)			
		end
		return result
	end
	return func(x_pcall(f, ...))
end

--对应proto 里的错误结构
function getErrorTable(message_name, error_code)
	return {[message_name]={
		error_code=error_code,
		error_msg=lang:get(error_code),
	}}
end

--中断当前协程，返回错误给客户端
--condition=false 时触发断言
--消息名 对应proto中 如
--错误id 对应文字在 ch.lua中
function assertEx(condition, message_name, error_code)
	assert(condition, getErrorTable(message_name, error_code))
end

--直接触发断言
function assertFalse(message_name, error_code)
	assert(false, getErrorTable(message_name, error_code))
end



