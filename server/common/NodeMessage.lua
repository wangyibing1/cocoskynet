--
-- Author:      name
-- DateTime:    2018-04-27 10:59:15
-- Description: 节点间消息转发，服务间消息转发


local skynet = require "skynet"
local log = require "Logger"
local wsnetpack = require "websocketnetpack"
local netpack = require "skynet.netpack"
local socket = require "skynet.socket"
local cluster = require "skynet.cluster"
local MessagePack = require "MessagePack"


---------------------------------------------------------
-- class
---------------------------------------------------------
local NodeMessage = class("NodeMessage")

function NodeMessage:ctor()
	self.message_pack = MessagePack.new() --消息编码

	self.logger_server = skynet.getenv("logger_cluster_id")
	self.db_server = skynet.getenv("db_cluster_id")
	self.zone_server = skynet.getenv("zone_cluster_id")	
	self.node_server = skynet.getenv("node_cluster_id")		
	self.robot_server = skynet.getenv("robot_cluster_id")
end

--如果要发送pb消息给客户端要初始化pb
function NodeMessage:initProto(pbc_env)
	self.message_pack:initProto(pbc_env)
end

--
function NodeMessage:getMessagePack()
	return self.message_pack
end

--客户端消息解包
function NodeMessage:unpackClient(...)
	return self.message_pack:unpackClient(...)
end

--发送消息给客户端
--contype 连接的类型socket websocket
function NodeMessage:sendClient(fd, contype, message_name, body)
	-- print("________________sendClient__",fd, contype, main_id, sub_id, body)
	--NET_MSG_HEADER(24) + NET_MSG_ROUTER(28) + NET_MSG_COMMOND(12) + PB
    print("___sendClient___",message_name)    
	local str = self.message_pack:packClientByName(message_name, body)
	self:writePackage(fd,contype,str)

end

--写入socket
function NodeMessage:writePackage( fd, contype, msg )
	local tmpmsg, sz
	if contype == "websocket" then
		tmpmsg, sz = wsnetpack.pack(msg)
	elseif contype == "socket" then
		tmpmsg, sz = netpack.pack(msg)	
	else
		tmpmsg, sz = netpack.pack(msg)
	end
	if not tmpmsg or not sz then 
		log.error('writePackage error__tmpmsg=nil or sz=nil')
		return false
	end
	local ok,err = pcall(socket.write, fd, tmpmsg, sz)
	if not ok then
		log.error('writePackage faild:'..err)
		return false
	end
	return true
end


---------------------------------------------------------
-- 不同进程服务间发送消息
---------------------------------------------------------
--异步发送消息请求 node:节点名, address服务名, cmd消息名
function NodeMessage:sendNode( node, address, cmd, ... )
	-- print("############", node, address, cmd, ... )
	local ok, result = x_pcall(cluster.send, node, address, cmd, ...)
	if not ok then
		log.error('##############NodeMessage.send faild:',result)
	end
	return result
end

--同步发送消息请求
function NodeMessage:callNode( node, address, cmd, ... )	
	-- print("________callNode ___",node, address, cmd)
	local ok,result = pcall(cluster.call, node, address, cmd, ...)	
	if not ok then
		log.error("############NodeMessage.call cmd:"..cmd.." faild:", result,node, address, cmd, ...)
		return false
	end
	return result
end

--异步发送到指定节点的.proxy服务
function NodeMessage:sendNodeProxy(node, cmd, ...)
	self:sendNode(node,'.proxy',cmd,...)
end

--同步发送到指定节点的.proxy服务
function NodeMessage:callNodeProxy(node, cmd, ...)
	return self:callNode(node,'.proxy',cmd,...)
end

--注册消息
function NodeMessage:sendNodeMessage(node, cmd, ...)
	self:sendNodeProxy(node,'node_message', cmd,...)
end
--
function NodeMessage:callNodeMessage(node, cmd, ...)
	return self:callNodeProxy(node,'node_message', cmd,...)
end



---------------------------------------------------------
-- 同进程服务间发送消息
---------------------------------------------------------
--同步给服务发消息
function NodeMessage:callService(servicename,cmd,...)
	-- print("____servicename,",servicename)
	return skynet.call(servicename,'lua',cmd,...)
end
--异步给服务发消息
function NodeMessage:sendService(servicename,cmd,...)
	skynet.send(servicename,'lua',cmd,...)
end
--异步发送到指定节点的.proxy服务
function NodeMessage:sendProxy(cmd, ...)
	self:sendService('.proxy',cmd,...)
end
--同步发送到指定节点的.proxy服务
function NodeMessage:callProxy(cmd, ...)
	return self:callService('.proxy',cmd,...)
end


---------------------------------------------------------
-- db进程间发送消息
---------------------------------------------------------
function NodeMessage:sendDbProxy(cmd, ...)
	self:sendNode(self.db_server,'.proxy',cmd,...)
end
function NodeMessage:callDbProxy(cmd, ...)
	return self:callNode(self.db_server,'.proxy',cmd,...)
end
--直接操作redis
function NodeMessage:callDbRedis(cmd, ...)
	return self:callDbProxy('executeRedis',cmd,...)
end
--直接操作mysql 传入sql名句, 用于复杂的操作
function NodeMessage:callDbMySql(sql)
	return self:callDbProxy('executeMySql', sql)
end
--传入table的方式操作mysql
function NodeMessage:callDbMySqlEx(cmd, tbname, field_values, where_field_values, async)
	return self:callDbProxy('executeMySqlEx',cmd, tbname, field_values, where_field_values, async)
end

---------------------------------------------------------
-- zone进程间发送消息
---------------------------------------------------------
function NodeMessage:sendZoneProxy(cmd, ...)
	self:sendNode(self.zone_server,'.proxy',cmd,...)
end
function NodeMessage:callZoneProxy(cmd, ...)
	return self:callNode(self.zone_server,'.proxy',cmd,...)
end
---------------------------------------------------------
-- gate进程间发送消息
---------------------------------------------------------
function NodeMessage:sendUser(node, user_id, cmd, ...)
	-- print("______sendUser(____", user_id, cmd)
	self:sendNodeProxy(node, 'message_to_user_nt', user_id, cmd, ...)
end
function NodeMessage:sendUsers(node, users, cmd, ...)
	-- print("______sendUsers(____", users, cmd)
	self:sendNodeProxy(node, 'message_to_users_nt', users, cmd, ...)
end


---------------------------------------------------------
-- robotserver间发送消息
---------------------------------------------------------
function NodeMessage:sendRobotProxy(cmd, ...)
	self:sendNodeProxy(self.robot_server, cmd, ...)
end

---------------------------------------------------------
-- nodeserver间发送消息
---------------------------------------------------------
function NodeMessage:sendNodeServerProxy(cmd, ...)
	if not self.node_server then 
		return 
	end
	self:sendNodeProxy(self.node_server, cmd, ...)
end
function NodeMessage:sendToServer(cmd, ...)
	self:sendNodeServerProxy("send_to_server", cmd, ...)
end


return NodeMessage