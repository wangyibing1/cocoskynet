--
-- @Author:      name
-- @DateTime:    2018-03-30 23:05:48
-- @Description: 用户信息表 以 username 作为 key
local Entity = require "entitybase.Entity"

local tbname = 'server_list'
local TB = class(tbname, Entity)

function TB:ctor(command)
	TB.super.ctor(self)
	self.tbname = tbname	
	self.command = command
	self.msg = command.message_dispatch
	--注册消息
	self:register()
end

function TB:register()
	self.msg:registerSelf('load_'..tbname,handler(self,self.load))
	self.msg:registerSelf('add_'..tbname,handler(self,self.add))
	self.msg:registerSelf('get_'..tbname,handler(self,self.get))	
	self.msg:registerSelf('delete_'..tbname,handler(self,self.delete))
	self.msg:registerSelf('update_'..tbname,handler(self,self.update))
	self.msg:registerSelf('reload_'..tbname,handler(self,self.reload))
	self.msg:registerSelf('unload_'..tbname,handler(self,self.unload))	


end




return TB
