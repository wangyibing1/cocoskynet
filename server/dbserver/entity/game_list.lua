--
-- @Author:      name
-- @DateTime:    2018-03-30 23:05:48
-- @Description: 
local Entity = require "entitybase.Entity"

local name = 'game_list'
local tbname = 'game_list'
local TB = class(name, Entity)

function TB:ctor(command)
	TB.super.ctor(self)
	self.tbname = tbname
	self.command = command
	self.msg = command.message_dispatch
	--注册消息
	self:register()
end

function TB:register()
	-- self.msg:registerSelf('add_'..name,handler(self,self.add))
	self.msg:registerSelf('load_'..name,handler(self,self.load))	
	-- self.msg:registerSelf('delete_'..name,handler(self,self.delete))
	-- self.msg:registerSelf('update_'..name,handler(self,self.update))
	-- self.msg:registerSelf('reload_'..name,handler(self,self.reload))
	-- self.msg:registerSelf('unload_'..name,handler(self,self.unload))	

end




return TB
