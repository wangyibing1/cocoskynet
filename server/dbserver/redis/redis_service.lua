--
-- @Author:      name
-- @DateTime:    2018-03-30 23:05:48
-- @Description: redis操作
require "skynet.manager"
local service_name = "redis_service"
local skynet = require "skynet"
local MessageDispatch = require "MessageDispatch"
local MessageHandler = require("redis.MessageHandler")


local function init()
	local message = MessageDispatch.new()	
	local message_handler = MessageHandler.new(message)
	-- lua消息派发
	skynet.dispatch("lua",message:dispatch())	
end


-- 启动服务
skynet.start(function()
	init()
	skynet.register(service_name)
end)

