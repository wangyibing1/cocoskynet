local Factory = class('Factory')


function Factory:ctor(command)
	self.command = command
	self.entities = {}		-- 保存实体对象
end

-- 工厂方法，获取具体对象，name为表名
function Factory:get(name)
	if self.entities[name] then
		return self.entities[name]
	end

	local Entity = require("entity."..name)
	self.entities[name] = Entity.new(self.command)
	return self.entities[name]
end

return Factory
