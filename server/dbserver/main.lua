--
-- @Author:      name
-- @DateTime:    2018-04-20 21:48:12
-- @Description: 集群节点起动与配置

local skynet = require "skynet"

skynet.start(function()

	--数据库,加入需要的表
	local tables = {
		{name="user", pk='user_id' },
	}
	--主服务
	local manager = skynet.uniqueservice("manager_service")
	skynet.call(manager, "lua", "start", tables)
	
    skynet.exit()
end)

