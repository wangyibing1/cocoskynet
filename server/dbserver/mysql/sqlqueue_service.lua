-- 把sql消息发到队列里

local skynet = require "skynet"
require "skynet.manager"
local MessageDispatch = require "MessageDispatch"

local Command = class()
local message_dispatch = MessageDispatch.new()

function Command:ctor()
    self.message_dispatch = message_dispatch
    self.queue = {}
    self:register()
end

function Command:register()
    self.message_dispatch:register("add_sql", handler(self, self.addSql))
end

function Command:loop()
    while true do       
        if next(self.queue) then
            -- skynet.send(skynet.self(), 'lua', 'tick')
            local sql = table.remove(self.queue, 1)
            skynet.send("mysql_service", "lua", "execute", sql, true)
        end
        skynet.sleep(30)--
    end
end

function Command:addSql(sql)
    table.insert(self.queue, sql)
end

local command = Command.new()


skynet.start(function()
    skynet.dispatch("lua", message_dispatch:dispatch())
    skynet.register(SERVICE_NAME)
end)

