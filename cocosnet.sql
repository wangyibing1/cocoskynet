/*
Navicat MySQL Data Transfer

Source Server         : 193.112.104.19
Source Server Version : 50726
Source Host           : 193.112.104.19:3306
Source Database       : cocosnet

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2019-09-07 20:36:58
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for game_category
-- ----------------------------
DROP TABLE IF EXISTS `game_category`;
CREATE TABLE `game_category` (
  `category_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(16) DEFAULT NULL,
  `ident` varchar(16) DEFAULT NULL,
  `resource_name` varchar(64) DEFAULT NULL,
  `resource_url` varchar(255) DEFAULT NULL,
  `index_url` varchar(255) DEFAULT NULL,
  `version` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COMMENT='游戏种类表';

-- ----------------------------
-- Records of game_category
-- ----------------------------
INSERT INTO `game_category` VALUES ('1', '街机类', 'slot', 'slot_common', 'https://static.ppro.98078.net//global/list/slot/slot_common/slot_common.zip?v=0.0.0.0', '', '0.0.0.0');
INSERT INTO `game_category` VALUES ('2', '捕猎类', 'fish', 'fish_common', 'https://static.ppro.98078.net//global/list/fish/fish_common/fish_common.zip?v=2019.03.08.18.57.06', 'index.html', '2019.03.08.18.57.06');
INSERT INTO `game_category` VALUES ('3', '扑克类', 'poker', 'poker_common', 'https://static.ppro.98078.net//global/list/poker/poker_common/poker_common.zip?v=2019.04.19.18.11.38', 'index.html', '2019.04.19.18.11.38');
INSERT INTO `game_category` VALUES ('4', '水果机类', 'fruit', 'fruit_common', 'https://static.ppro.98078.net//global/list/fruit/fruit_common/fruit_common.zip?v=0.0.0.0', '', '0.0.0.0');

-- ----------------------------
-- Table structure for game_classify
-- ----------------------------
DROP TABLE IF EXISTS `game_classify`;
CREATE TABLE `game_classify` (
  `classify_id` int(11) NOT NULL COMMENT '游戏分类id',
  `category_id` int(11) DEFAULT NULL COMMENT '游戏种类id',
  `name` varchar(20) CHARACTER SET utf8mb4 NOT NULL DEFAULT '' COMMENT '子游戏名称',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`classify_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of game_classify
-- ----------------------------
INSERT INTO `game_classify` VALUES ('1', '3', '牛牛', '2019-05-17 18:49:48', '2019-05-17 18:49:48');
INSERT INTO `game_classify` VALUES ('2', '3', '扎金花', '2019-05-17 18:49:48', '2019-05-17 18:49:48');

-- ----------------------------
-- Table structure for game_config
-- ----------------------------
DROP TABLE IF EXISTS `game_config`;
CREATE TABLE `game_config` (
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `game_id` int(8) NOT NULL,
  `group_id` int(8) NOT NULL,
  `game_name` varchar(30) DEFAULT NULL,
  `game_code` varchar(30) DEFAULT NULL COMMENT '游戏代码',
  `game_genre` int(8) DEFAULT NULL,
  `dynamic_join` int(1) DEFAULT NULL,
  `offline_trustee` int(1) DEFAULT NULL,
  `server_rule` int(11) DEFAULT NULL COMMENT '服务器规则',
  `desk_count` int(11) DEFAULT NULL,
  `seat_count` tinyint(4) DEFAULT NULL,
  `base_score` int(16) DEFAULT NULL,
  `revenue_ratio` int(4) DEFAULT NULL COMMENT '收入比',
  `service_cost` int(4) DEFAULT NULL COMMENT '服务费',
  `min_enter_score` int(16) DEFAULT NULL,
  `max_enter_score` int(16) DEFAULT NULL,
  `restrict_score` int(16) DEFAULT NULL,
  `min_game_score` int(16) DEFAULT NULL,
  `min_enter_member` int(16) DEFAULT NULL,
  `max_enter_member` int(16) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`game_id`,`group_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='游戏配置表';

-- ----------------------------
-- Records of game_config
-- ----------------------------
INSERT INTO `game_config` VALUES ('3', '1', '1', '牛牛', 'niuniu', '1', '0', '1', '0', '10', '6', '1', '10', '0', '0', '10000000', '0', '1', '0', '0');
INSERT INTO `game_config` VALUES ('4', '1', '2', '牛牛', 'niuniu', '1', '0', '1', '0', '3', '6', '1', '10', '0', '0', '10000000', '0', '1', '0', '0');
INSERT INTO `game_config` VALUES ('5', '2', '1', '金花', 'zjh', '2', '0', '1', '0', '3', '5', '10', '10', '0', '100', '5000', '0', '1', '0', '0');
INSERT INTO `game_config` VALUES ('6', '2', '2', '金花', 'zjh', '2', '0', '1', '0', '3', '5', '50', '10', '0', '1000', '80000', '0', '1', '0', '0');

-- ----------------------------
-- Table structure for game_list
-- ----------------------------
DROP TABLE IF EXISTS `game_list`;
CREATE TABLE `game_list` (
  `game_id` int(20) NOT NULL COMMENT '游戏id',
  `name` varchar(20) NOT NULL DEFAULT '' COMMENT '游戏名称',
  `classify_id` varchar(8) NOT NULL DEFAULT '' COMMENT '游戏分类id',
  `category_id` int(1) NOT NULL DEFAULT '1' COMMENT '游戏种类id',
  `code` varchar(16) NOT NULL DEFAULT '' COMMENT '游戏代码',
  `vendor` varchar(18) DEFAULT NULL COMMENT '游戏提供商',
  `service_id` bigint(20) NOT NULL COMMENT '第三方游戏标识id',
  `round_handler` varchar(32) DEFAULT NULL COMMENT '牌局处理器标志',
  `game_short_code` varchar(32) DEFAULT NULL COMMENT '游戏简化代码，主要用于游戏详情解析识别url',
  `game_url` varchar(256) DEFAULT NULL COMMENT '游戏地址',
  `is_hot` tinyint(1) DEFAULT NULL COMMENT '是否热门',
  `is_new` tinyint(1) DEFAULT NULL COMMENT '是否新品',
  `is_recommend` tinyint(1) DEFAULT NULL COMMENT '是否推荐',
  `official_sort` int(11) DEFAULT NULL COMMENT '官方排序',
  `self_sort` int(11) DEFAULT NULL COMMENT '自有排序',
  `square_img` varchar(256) DEFAULT NULL COMMENT '方形游戏图片',
  `cycle_img` varchar(256) DEFAULT NULL COMMENT '圆形游戏图片',
  `big_img` varchar(256) DEFAULT NULL COMMENT '游戏大图片',
  `zip_url` varchar(256) DEFAULT NULL COMMENT 'zip包地址',
  `entrance` varchar(64) DEFAULT NULL COMMENT '入口地址',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_allow_vertical` int(1) NOT NULL DEFAULT '1' COMMENT '是否支持竖版：0不支持、1支持',
  `is_exit` int(1) NOT NULL DEFAULT '0' COMMENT '是否有返回大厅按钮：0无、1有',
  PRIMARY KEY (`game_id`),
  UNIQUE KEY `unique_code_service_id` (`code`,`service_id`) USING BTREE,
  UNIQUE KEY `unique_short_counique_short_code` (`game_short_code`) USING BTREE,
  UNIQUE KEY `unique_handler` (`round_handler`) USING BTREE,
  KEY `IDX_CATE` (`classify_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='游戏';

-- ----------------------------
-- Records of game_list
-- ----------------------------
INSERT INTO `game_list` VALUES ('1', '通比牛牛', '1', '3', 'tbnn', 'fungame', '6504', 'poker_FG_TBNN', 'poker_tbnn', 'https://h5.ppro.98078.net/game?type=h5&gamecode=tbnn&language=zh-cn', '0', '1', '0', '29992', '29992', 'https://static.ppro.98078.net/global/files/images/2018091823551152730.png', 'https://static.ppro.98078.net/global/files/images/2018091905284982262.png', '', 'https://static.ppro.98078.net/global/list/poker/6504/6504.zip?v=2019.04.17.11.40.09', 'index.html', '2019-04-15 13:40:01', '2019-04-15 13:40:01', '1', '1');
INSERT INTO `game_list` VALUES ('2', '经典炸金花', '2', '3', 'zjhdr', 'fungame', '6302', 'poker_FG_ZJH_JD', 'poker_zjh_jd', 'https://h5.ppro.98078.net/game?type=h5&gamecode=zjhdr&language=zh-cn', '0', '1', '0', '29999', '29999', 'https://static.ppro.98078.net/global/files/images/2018091902243121499.png', 'https://static.ppro.98078.net/global/files/images/2018091905283613079.png', '', 'https://static.ppro.98078.net/global/list/poker/6302/6302.zip?v=2019.04.16.18.30.45', 'index.html', '2019-04-15 13:40:01', '2019-04-16 17:12:48', '1', '1');

-- ----------------------------
-- Table structure for record_game
-- ----------------------------
DROP TABLE IF EXISTS `record_game`;
CREATE TABLE `record_game` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `user_id` bigint(20) NOT NULL COMMENT '玩家id',
  `server_id` int(11) NOT NULL COMMENT '服务节点id\r\n',
  `desk_id` int(11) NOT NULL COMMENT '桌子id',
  `desk_born_time` bigint(20) NOT NULL COMMENT '桌子生成时间',
  `round_count` bigint(20) NOT NULL COMMENT '游戏轮数',
  `game_id` int(11) DEFAULT NULL COMMENT '游戏id',
  `group_id` int(11) DEFAULT NULL COMMENT '分组id',
  `classify_id` int(4) NOT NULL COMMENT '游戏分类id',
  `before_gold` decimal(11,0) NOT NULL COMMENT '游戏前金币',
  `after_gold` decimal(20,0) NOT NULL COMMENT '游戏后金币',
  `bet` decimal(20,0) NOT NULL COMMENT '下注金币',
  `win` decimal(20,0) NOT NULL COMMENT '赢得金币',
  `is_banker` tinyint(20) DEFAULT NULL COMMENT '是否是庄家',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_for_game_data` (`user_id`,`server_id`,`desk_id`,`desk_born_time`,`round_count`) USING BTREE COMMENT '唯一标识一条用户游戏数据',
  KEY `query_win_info` (`create_time`,`user_id`,`desk_id`,`win`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='牛牛游戏数据记录表';

-- ----------------------------
-- Records of record_game
-- ----------------------------

-- ----------------------------
-- Table structure for record_login
-- ----------------------------
DROP TABLE IF EXISTS `record_login`;
CREATE TABLE `record_login` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `user_id` bigint(20) NOT NULL COMMENT '玩家id',
  `ip` varchar(32) NOT NULL COMMENT '登陆IP',
  `area` varchar(32) NOT NULL COMMENT '登陆地区',
  `mac` varchar(64) NOT NULL COMMENT '登陆mac地址',
  `gold` decimal(11,0) NOT NULL COMMENT '玩家登陆时携带的金币',
  `diamond` decimal(20,0) NOT NULL COMMENT '玩家登陆时携带的钻石',
  `version` varchar(16) NOT NULL COMMENT '版本信息',
  `phone_info` varchar(16) NOT NULL COMMENT '手机型号',
  PRIMARY KEY (`id`),
  KEY `query_user_info` (`user_id`,`area`,`ip`,`phone_info`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='登陆日志表';

-- ----------------------------
-- Records of record_login
-- ----------------------------

-- ----------------------------
-- Table structure for record_money
-- ----------------------------
DROP TABLE IF EXISTS `record_money`;
CREATE TABLE `record_money` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `user_id` bigint(20) NOT NULL COMMENT '玩家id',
  `gold` decimal(20,0) DEFAULT NULL COMMENT '当前金币数',
  `gold_change` decimal(20,0) DEFAULT NULL COMMENT '金币更改值',
  `diamond` bigint(20) DEFAULT NULL COMMENT '当前钻石数',
  `diamond_change` bigint(11) DEFAULT NULL COMMENT '钻石修改值',
  `score` bigint(20) DEFAULT NULL COMMENT '当前积分',
  `score_change` bigint(20) DEFAULT NULL COMMENT '积分修改值',
  `type` tinyint(4) NOT NULL COMMENT '流水类型',
  `shop_id` int(11) DEFAULT '0' COMMENT '商品id',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `summary_gold` (`create_time`,`user_id`,`type`,`gold`,`gold_change`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户流水表';

-- ----------------------------
-- Records of record_money
-- ----------------------------

-- ----------------------------
-- Table structure for server_list
-- ----------------------------
DROP TABLE IF EXISTS `server_list`;
CREATE TABLE `server_list` (
  `server_id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '服务器名',
  `type` varchar(30) NOT NULL DEFAULT '' COMMENT '服务器类型',
  `gate_id` int(11) DEFAULT '0',
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `online` tinyint(1) DEFAULT '0' COMMENT '在线标记',
  `ip` varchar(30) NOT NULL DEFAULT '127.0.0.1' COMMENT '服务器ip',
  `port` int(4) DEFAULT '0' COMMENT '服务器端口',
  `socket_port` int(4) DEFAULT NULL,
  `websocket_port` int(4) DEFAULT NULL,
  `game_id` int(4) DEFAULT '0' COMMENT '游戏id',
  `group_id` int(4) DEFAULT '0' COMMENT '场次id',
  `debug_port` int(4) DEFAULT NULL,
  `max_client` int(11) DEFAULT '1000' COMMENT '最大人数',
  `online_client` int(11) DEFAULT '0' COMMENT '在线人数',
  PRIMARY KEY (`server_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='服务器信息表';

-- ----------------------------
-- Records of server_list
-- ----------------------------
INSERT INTO `server_list` VALUES ('1', '登录服网关', 'gateserver', '0', '1', '0', '127.0.0.1', '10011', '10012', '10013', '0', '0', null, '1000', '0');
INSERT INTO `server_list` VALUES ('2', '游戏服网关', 'gateserver', '0', '1', '0', '127.0.0.1', '10021', '10022', '10023', '0', '0', null, '1000', '0');
INSERT INTO `server_list` VALUES ('3', '区域服网关', 'gateserver', '0', '1', '0', '127.0.0.1', '10031', '10032', '10033', '0', '0', null, '1000', '0');
INSERT INTO `server_list` VALUES ('101', '区域1', 'zoneserver', '3', '1', '0', '127.0.0.1', '10101', null, null, '0', '0', null, '1000', '0');
INSERT INTO `server_list` VALUES ('102', '日记服', 'loggerserver', '3', '1', '0', '127.0.0.1', '10201', null, null, '0', '0', null, '1000', '0');
INSERT INTO `server_list` VALUES ('103', 'db服', 'dbserver', '3', '1', '0', '127.0.0.1', '10301', null, null, '0', '0', null, '1000', '0');
INSERT INTO `server_list` VALUES ('201', '登录1', 'loginserver', '1', '1', '0', '127.0.0.1', '20101', null, null, '0', '0', null, '1000', '0');
INSERT INTO `server_list` VALUES ('301', '大厅1', 'hallserver', '2', '1', '0', '127.0.0.1', '30101', null, null, '0', '0', null, '1000', '0');
INSERT INTO `server_list` VALUES ('401', '游戏1', 'gameserver', '2', '0', '0', '127.0.0.1', '40101', null, null, '1', '1', null, '1000', '0');
INSERT INTO `server_list` VALUES ('402', '游戏2', 'gameserver', '2', '0', '0', '127.0.0.1', '40201', null, null, '2', '1', null, '1000', '0');
INSERT INTO `server_list` VALUES ('501', 'web', 'webserver', '0', '0', '0', '127.0.0.1', '50101', null, null, '0', '0', null, '1000', '0');

-- ----------------------------
-- Table structure for trade_wallet
-- ----------------------------
DROP TABLE IF EXISTS `trade_wallet`;
CREATE TABLE `trade_wallet` (
  `wallet_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '钱包id',
  `user_id` bigint(20) NOT NULL COMMENT '用户id',
  `balance` decimal(20,2) NOT NULL DEFAULT '1000.00' COMMENT '账户余额',
  `lock_amt` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '锁定额度',
  `lock_game_id` int(20) NOT NULL DEFAULT '0' COMMENT '锁定游戏id',
  `lock_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '锁定时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `openid` varchar(64) DEFAULT NULL COMMENT 'openid',
  PRIMARY KEY (`wallet_id`),
  KEY `idx_user_id_ident` (`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='钱包';

-- ----------------------------
-- Records of trade_wallet
-- ----------------------------

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `channel_id` int(11) DEFAULT '0' COMMENT '渠道标识',
  `domain_id` int(11) unsigned zerofill NOT NULL DEFAULT '00000000000' COMMENT '平台id',
  `username` varchar(64) CHARACTER SET utf8mb4 NOT NULL DEFAULT '' COMMENT '用户名',
  `gold` decimal(32,0) NOT NULL DEFAULT '1000' COMMENT '金币',
  `diamond` int(32) NOT NULL DEFAULT '0' COMMENT '钻石',
  `password` varchar(64) CHARACTER SET utf8mb4 NOT NULL DEFAULT '' COMMENT '密码',
  `mobile` varchar(11) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '手机号码',
  `is_robot` tinyint(1) DEFAULT '0' COMMENT '是否机器人',
  `nickname` varchar(16) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '昵称',
  `reg_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '注册时间',
  `reg_ip` varchar(15) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '注册ip',
  `reg_addr` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '注册ip地址',
  `last_login_ip` varchar(15) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '最后登陆ip',
  `last_login_addr` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '最后登陆ip地址',
  `last_login_time` datetime DEFAULT NULL COMMENT '最后登录时间',
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '账户状态（0:禁用,1启用)',
  `online` int(1) NOT NULL DEFAULT '0' COMMENT '在线状态(0:下线,1:上线)',
  `type` int(1) NOT NULL DEFAULT '2' COMMENT '用户类型(1:试玩或游客,2:正式)',
  `position` int(1) NOT NULL DEFAULT '0' COMMENT '所在位置(0未知1游戏大厅,2:游戏房间)',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `face_id` int(1) NOT NULL DEFAULT '1' COMMENT '头像类型',
  `safe_pwd` varchar(8) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '保险箱密码',
  `token` varchar(32) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '登录时产生的token',
  `invite_code` varchar(10) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '注册邀请码',
  `level_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '会员级别id',
  `integral` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '积分',
  `device` varchar(16) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '客户端类型：安卓、IOS、H5',
  `wx_openid` varchar(64) CHARACTER SET utf8mb4 DEFAULT NULL,
  `mac_addr` varchar(50) DEFAULT NULL COMMENT '客户端mac地址',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `mobile` (`mobile`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=121 DEFAULT CHARSET=utf8 COMMENT='用户表';

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', '0', '00000000000', '33333', '1000', '0', '123456', '13255556666', '0', 'world', '2019-04-18 22:57:24', '192.168.2.253', '0|0|0|内网IP|内网IP', '192.168.2.253', '0|0|0|内网IP|内网IP', '2019-04-19 00:21:52', '1', '0', '2', '1', '2019-04-18 22:57:24', '2019-04-19 16:51:33', '1', null, null, '7davx21k', '1', '0.00', null, null, null);
INSERT INTO `user` VALUES ('2', '0', '00000000000', '15920094537', '1000', '0', '123456', '15920094537', '0', '15920094537', '2019-04-18 23:20:15', '127.0.0.1', '0|0|0|内网IP|内网IP', '192.168.2.190', '0|0|0|内网IP|内网IP', '2019-04-26 09:57:33', '1', '0', '2', '1', '2019-04-18 23:20:15', '2019-04-19 05:48:01', '1', '2222', null, 'h8cgregr', '1', '50.00', null, null, null);
INSERT INTO `user` VALUES ('3', '0', '00000000000', '18676791424', '1000', '0', '123456', '18676791424', '0', '18676791424', '2019-04-18 23:23:13', '127.0.0.1', '0|0|0|内网IP|内网IP', '127.0.0.1', '0|0|0|内网IP|内网IP', '2019-04-19 00:21:52', '1', '0', '2', '2', '2019-04-18 23:23:13', '2019-04-19 04:28:01', '1', '123456', null, 'ap9bspxd', '1', '0.00', null, null, null);
INSERT INTO `user` VALUES ('4', '0', '00000000000', '862681035351125', '1000', '0', '', null, '0', null, '2019-04-19 00:11:12', '192.168.2.252', '0|0|0|内网IP|内网IP', '192.168.2.252', '0|0|0|内网IP|内网IP', '2019-04-19 00:21:52', '1', '0', '2', '1', '2019-04-19 00:11:12', '2019-04-19 03:04:01', '1', null, null, 'l4hqbiql', '1', '0.00', null, null, null);
INSERT INTO `user` VALUES ('5', '0', '00000000000', '13545321242', '1000', '0', 'songss', '13545321242', '0', '13545321242', '2019-04-19 00:21:27', '183.17.230.168', '中国|0|广东省|深圳市|电信', '183.17.230.168', '中国|0|广东省|深圳市|电信', '2019-04-19 00:21:52', '1', '0', '2', '0', '2019-04-19 00:21:27', '2019-04-19 04:58:32', '1', null, null, 'gaqsusze', '1', '0.00', null, null, null);
INSERT INTO `user` VALUES ('6', '0', '00000000000', '15920094533', '1000', '0', 'songss', '15920094533', '0', '15920094533', '2019-04-19 00:25:09', '127.0.0.1', '0|0|0|内网IP|内网IP', '127.0.0.1', '0|0|0|内网IP|内网IP', '2019-04-19 00:21:52', '1', '0', '2', '0', '2019-04-19 00:25:09', '2019-04-19 04:58:44', '1', null, null, 'ip31ma57', '1', '0.00', null, null, null);
INSERT INTO `user` VALUES ('7', '0', '00000000000', '15897961440', '1000', '0', '123456', '15897961440', '0', '15897961440', '2019-04-19 00:28:34', '117.132.192.162', '中国|0|0|0|移动', '192.168.2.253', '0|0|0|内网IP|内网IP', '2019-04-24 20:01:48', '1', '0', '2', '1', '2019-04-19 00:28:34', '2019-04-19 02:05:01', '1', '123456', null, 'xuqgjels', '1', '50.00', null, null, null);
INSERT INTO `user` VALUES ('8', '0', '00000000000', '15218711781', '1000', '0', '123456', '12920094537', '0', '12920094537', '2019-04-19 02:34:45', '192.168.2.252', '0|0|0|内网IP|内网IP', '192.168.2.233', '0|0|0|内网IP|内网IP', '2019-04-24 12:01:15', '1', '0', '2', '1', '2019-04-19 02:34:45', '2019-04-19 05:39:01', '1', null, null, 'eo6yi9z7', '1', '0.00', null, null, null);
INSERT INTO `user` VALUES ('9', '0', '00000000000', '17123456789', '1000', '0', 'lolo', '12787677', '0', '', '2019-04-22 12:59:59', '', null, null, null, '2019-04-22 12:59:59', '1', '0', '2', '0', '2019-04-22 20:48:37', '2019-04-22 20:48:37', '1', null, null, 'xcsd', '12', '0.00', null, null, null);
INSERT INTO `user` VALUES ('11', '0', '00000000000', 'sbsbs', '1000', '0', 'lolos', '12787677s', '0', '', '2019-04-22 12:59:59', '', null, null, null, '2019-04-22 12:59:59', '1', '0', '2', '0', '2019-04-22 20:57:37', '2019-04-22 20:57:37', '1', null, null, 'xcsds', '12', '0.00', null, null, null);
INSERT INTO `user` VALUES ('12', '0', '00000000000', '353288085807042', '1000', '0', '', null, '0', null, '2019-04-23 11:03:16', '192.168.2.184', '0|0|0|内网IP|内网IP', '192.168.2.184', '0|0|0|内网IP|内网IP', '2019-04-23 11:07:51', '1', '0', '2', '1', '2019-04-23 11:03:16', '2019-04-23 11:03:16', '1', null, null, 'sa702dh7', '1', '0.00', null, null, null);
INSERT INTO `user` VALUES ('13', '0', '00000000000', '869019032840373', '1000', '0', '', null, '0', null, '2019-04-23 11:26:06', '192.168.2.220', '0|0|0|内网IP|内网IP', '192.168.2.220', '0|0|0|内网IP|内网IP', '2019-04-23 15:35:58', '1', '0', '2', '1', '2019-04-23 11:26:06', '2019-04-23 11:26:06', '1', null, null, 'tod5mbcs', '1', '0.00', null, null, null);
INSERT INTO `user` VALUES ('14', '0', '00000000000', '865166020610237', '1000', '0', '', null, '0', null, '2019-04-23 13:59:09', '192.168.2.176', '0|0|0|内网IP|内网IP', '192.168.2.176', '0|0|0|内网IP|内网IP', '2019-04-23 17:48:54', '1', '0', '2', '2', '2019-04-23 13:59:09', '2019-04-23 13:59:09', '1', null, null, '9oyin0kb', '1', '0.00', null, null, null);
INSERT INTO `user` VALUES ('15', '0', '00000000000', '349149782637692423081244658322', '1000', '0', '', null, '0', null, '2019-04-23 13:59:32', '192.168.2.252', '0|0|0|内网IP|内网IP', '192.168.2.190', '0|0|0|内网IP|内网IP', '2019-04-24 22:14:49', '1', '0', '2', '1', '2019-04-23 13:59:32', '2019-04-23 13:59:32', '1', '12345678', null, 'ekmr9u5x', '1', '0.00', null, null, null);
INSERT INTO `user` VALUES ('16', '0', '00000000000', '865166020682020', '1000', '0', '', null, '0', null, '2019-04-23 14:00:48', '192.168.2.167', '0|0|0|内网IP|内网IP', '192.168.2.234', '0|0|0|内网IP|内网IP', '2019-04-25 15:41:26', '1', '0', '2', '1', '2019-04-23 14:00:48', '2019-04-23 14:00:48', '1', null, null, 'fc91u4jz', '1', '0.00', null, null, null);
INSERT INTO `user` VALUES ('17', '0', '00000000000', '861063043182928', '1000', '0', '', null, '0', null, '2019-04-23 14:03:24', '192.168.2.170', '0|0|0|内网IP|内网IP', '192.168.2.170', '0|0|0|内网IP|内网IP', '2019-04-23 15:18:10', '1', '0', '2', '1', '2019-04-23 14:03:24', '2019-04-23 14:03:24', '1', '123456', null, 'i3r09ztv', '1', '0.00', null, null, null);
INSERT INTO `user` VALUES ('18', '0', '00000000000', '18664578918', '1000', '0', '123456', '15012965089', '0', '15012965089', '2019-04-23 22:09:24', '192.168.2.190', '0|0|0|内网IP|内网IP', '192.168.2.252', '0|0|0|内网IP|内网IP', '2019-04-26 09:35:28', '1', '0', '2', '1', '2019-04-23 22:09:24', '2019-04-23 22:09:24', '1', '1234', null, 'zz112ej3', '1', '0.00', null, null, null);
INSERT INTO `user` VALUES ('19', '0', '00000000000', '15012965346', '1000', '0', '123456', '15012965346', '0', '15012965346', '2019-04-23 22:09:47', '192.168.2.190', '0|0|0|内网IP|内网IP', '192.168.2.190', '0|0|0|内网IP|内网IP', '2019-04-25 21:10:48', '1', '0', '2', '1', '2019-04-23 22:09:47', '2019-04-23 22:09:47', '1', '12345678', null, 'ldsiqpat', '1', '0.00', null, null, null);
INSERT INTO `user` VALUES ('20', '0', '00000000000', '15012965344', '1000', '0', '123456', '15012965344', '0', '15012965344', '2019-04-24 21:55:46', '192.168.2.190', '0|0|0|内网IP|内网IP', '192.168.2.190', '0|0|0|内网IP|内网IP', '2019-04-24 22:42:12', '1', '0', '2', '1', '2019-04-24 21:55:46', '2019-04-24 21:55:46', '1', null, null, 'yd9hnxzb', '1', '0.00', null, null, null);
INSERT INTO `user` VALUES ('21', '0', '00000000000', 'bb', '1000', '0', 'zz', '176545626', '0', '', '2019-04-25 12:21:54', '', null, null, null, null, '1', '0', '2', '0', '2019-04-25 10:42:52', '2019-04-25 10:42:52', '1', null, null, '2121', '12', '0.00', null, null, null);
INSERT INTO `user` VALUES ('22', '0', '00000000000', 'test20', '1000', '0', '123456', '', '0', 'test', '2019-04-30 11:04:00', '', null, null, null, null, '0', '0', '2', '0', '2019-04-30 11:04:00', '2019-04-30 11:04:00', '1', null, null, '0', '0', '0.00', '0', null, null);
INSERT INTO `user` VALUES ('34', '0', '00000000000', 'test48', '1000', '0', '123456', null, '0', 'test', '2019-04-30 11:53:17', '', null, null, null, null, '0', '0', '2', '0', '2019-04-30 11:53:17', '2019-04-30 11:53:17', '1', null, null, null, '0', '0.00', '0', null, null);
INSERT INTO `user` VALUES ('35', '0', '00000000000', 'test81', '1000', '0', '123456', null, '0', 'test', '2019-04-30 11:54:13', '', null, null, null, null, '0', '0', '2', '0', '2019-04-30 11:54:13', '2019-04-30 11:54:13', '1', null, null, null, '0', '0.00', '0', null, null);
INSERT INTO `user` VALUES ('36', '0', '00000000000', 'test89', '1000', '0', '123456', null, '0', 'test', '2019-04-30 13:54:55', '', null, null, null, null, '0', '0', '2', '0', '2019-04-30 13:54:55', '2019-04-30 13:54:55', '1', null, null, null, '0', '0.00', '0', null, null);
INSERT INTO `user` VALUES ('37', '0', '00000000000', 'test82', '1000', '0', '123456', null, '0', 'test', '2019-04-30 13:56:58', '', null, null, null, null, '0', '0', '2', '0', '2019-04-30 13:56:58', '2019-04-30 13:56:58', '1', null, null, null, '0', '0.00', '0', null, null);
INSERT INTO `user` VALUES ('38', '0', '00000000000', 'test15', '1000', '0', '123456', null, '0', 'test', '2019-04-30 13:58:25', '', null, null, null, null, '0', '0', '2', '0', '2019-04-30 13:58:25', '2019-04-30 13:58:25', '1', null, null, null, '0', '0.00', '0', null, null);
INSERT INTO `user` VALUES ('39', '0', '00000000000', 'test72', '1000', '0', '123456', null, '0', 'test', '2019-04-30 13:59:11', '', null, null, null, null, '0', '0', '2', '0', '2019-04-30 13:59:11', '2019-04-30 13:59:11', '1', null, 'MTU1OTIzMTU3Mw==', null, '0', '0.00', '0', null, null);
INSERT INTO `user` VALUES ('40', '0', '00000000000', 'test73', '1000', '0', '123456', null, '0', 'test', '2019-05-13 14:51:20', '', null, null, null, null, '0', '0', '2', '0', '2019-05-13 14:51:20', '2019-05-13 14:51:20', '1', null, 'MTU1OTIzMTU3NQ==', null, '0', '0.00', '0', null, null);
INSERT INTO `user` VALUES ('41', '0', '00000000000', 'test74', '1000', '0', '123456', null, '0', 'test', '2019-05-13 17:33:02', '', null, null, null, null, '0', '0', '2', '0', '2019-05-13 17:33:02', '2019-05-13 17:33:02', '1', null, 'MTU1OTI5ODg0Nw==', null, '0', '0.00', '0', null, null);
INSERT INTO `user` VALUES ('42', '0', '00000000000', 'test75', '1000', '0', '123456', null, '0', 'test', '2019-05-13 17:37:39', '', null, null, null, null, '0', '0', '2', '0', '2019-05-13 17:37:39', '2019-05-13 17:37:39', '1', null, 'MTU1OTI5ODg0OQ==', null, '0', '0.00', '0', null, null);
INSERT INTO `user` VALUES ('43', '0', '00000000000', 'test76', '1000', '0', '123456', null, '0', 'test', '2019-05-13 17:40:33', '', null, null, null, null, '0', '0', '2', '0', '2019-05-13 17:40:33', '2019-05-13 17:40:33', '1', null, null, null, '0', '0.00', '0', null, null);
INSERT INTO `user` VALUES ('44', '0', '00000000000', 'test77', '1000', '0', '123456', null, '0', 'test', '2019-05-13 17:42:10', '', null, null, null, null, '0', '0', '2', '0', '2019-05-13 17:42:10', '2019-05-13 17:42:10', '1', null, null, null, '0', '0.00', '0', null, null);
INSERT INTO `user` VALUES ('45', '0', '00000000000', 'test78', '1000', '0', '123456', null, '0', 'test', '2019-05-13 17:43:53', '', null, null, null, null, '0', '0', '2', '0', '2019-05-13 17:43:53', '2019-05-13 17:43:53', '1', null, null, null, '0', '0.00', '0', null, null);
INSERT INTO `user` VALUES ('46', '0', '00000000000', 'test79', '1000', '0', '123456', null, '0', 'test', '2019-05-13 17:46:25', '', null, null, null, null, '0', '0', '2', '0', '2019-05-13 17:46:25', '2019-05-13 17:46:25', '1', null, null, null, '0', '0.00', '0', null, null);
INSERT INTO `user` VALUES ('47', '0', '00000000000', 'test80', '1000', '0', '123456', null, '0', 'test', '2019-05-13 17:47:41', '', null, null, null, null, '0', '0', '2', '0', '2019-05-13 17:47:41', '2019-05-13 17:47:41', '1', null, 'MTU1NjUwNzg4Nw==', null, '0', '0.00', '0', null, null);
INSERT INTO `user` VALUES ('48', '0', '00000000000', 'test83', '1000', '0', '123456', null, '0', 'test', '2019-05-13 17:54:02', '', null, null, null, null, '0', '0', '2', '0', '2019-05-13 17:54:02', '2019-05-13 17:54:02', '1', null, 'MTU1NjUwODI5OA==', null, '0', '0.00', '0', null, null);
INSERT INTO `user` VALUES ('49', '0', '00000000000', 'test84', '1000', '0', '123456', null, '0', 'test', '2019-05-13 17:55:54', '', null, null, null, null, '0', '0', '2', '0', '2019-05-13 17:55:54', '2019-05-13 17:55:54', '1', null, 'MTU1NjUwODQyMA==', null, '0', '0.00', '0', null, null);
INSERT INTO `user` VALUES ('50', '0', '00000000000', '12324312', '1000', '0', '123456', null, '0', '12324312', '2019-05-14 11:52:03', '', null, null, null, null, '0', '0', '2', '0', '2019-05-14 11:52:03', '2019-05-14 11:52:03', '1', null, 'MTU1NjUzMTMzNw==', null, '0', '0.00', '1', null, '12324312');
INSERT INTO `user` VALUES ('51', '0', '00000000000', '12324313', '1000', '0', '123456', null, '0', '12324313', '2019-05-14 12:04:19', '', null, null, null, null, '0', '0', '2', '0', '2019-05-14 12:04:19', '2019-05-14 12:04:19', '1', null, 'MTU1NjUzMTM3OA==', null, '0', '0.00', '1', null, '12324313');
INSERT INTO `user` VALUES ('52', '0', '00000000000', '12324314', '10000', '0', '123456', null, '0', '12324314', '2019-05-14 12:05:06', '', null, null, null, null, '0', '0', '2', '0', '2019-05-14 12:05:06', '2019-05-14 12:05:06', '1', null, 'MTU1NjU0NTg4NA==', null, '0', '0.00', '1', null, '12324314');
INSERT INTO `user` VALUES ('54', '0', '00000000000', 'test16', '1000', '0', '123456', null, '0', 'test', '2019-05-16 10:59:33', '', null, null, null, null, '0', '0', '2', '0', '2019-05-16 10:59:33', '2019-05-16 10:59:33', '1', null, null, null, '0', '0.00', '0', null, null);
INSERT INTO `user` VALUES ('55', '0', '00000000000', 'test17', '1000', '0', '123456', null, '0', 'test', '2019-05-16 11:04:29', '', null, null, null, null, '0', '0', '2', '0', '2019-05-16 11:04:29', '2019-05-16 11:04:29', '1', null, null, null, '0', '0.00', '0', null, null);
INSERT INTO `user` VALUES ('56', '0', '00000000000', 'test18', '1000', '0', '123456', null, '0', 'test', '2019-05-16 11:07:14', '', null, null, null, null, '0', '0', '2', '0', '2019-05-16 11:07:14', '2019-05-16 11:07:14', '1', null, 'MTU1NjYxMzg5OQ==', null, '0', '0.00', '0', null, null);
INSERT INTO `user` VALUES ('57', '0', '00000000000', '123', '1000', '0', '', null, '0', '', '2019-05-16 11:33:47', '', null, null, null, null, '0', '0', '2', '0', '2019-05-16 11:33:47', '2019-05-16 11:33:47', '56', null, 'MTU1NjYxNTYyNQ==', null, '0', '0.00', '0', null, null);
INSERT INTO `user` VALUES ('58', '0', '00000000000', 'test19', '1000', '0', '123456', null, '0', 'test', '2019-05-16 11:38:35', '', null, null, null, null, '0', '0', '2', '0', '2019-05-16 11:38:35', '2019-05-16 11:38:35', '1', null, 'MTU1NjYxNTkzOA==', null, '0', '0.00', '0', null, null);
INSERT INTO `user` VALUES ('59', '0', '00000000000', '123123', '1000', '0', 'test1557978151', null, '0', '', '2019-05-16 11:42:32', '', null, null, null, null, '0', '0', '2', '0', '2019-05-16 11:42:32', '2019-05-16 11:42:32', '75', null, 'MTU1NjYxNjE5NA==', null, '0', '0.00', '0', null, null);
INSERT INTO `user` VALUES ('60', '0', '00000000000', '123456', '1000', '0', '123', null, '0', 'test1557985700', '2019-05-16 13:48:20', '', null, null, null, null, '0', '0', '2', '0', '2019-05-16 13:48:20', '2019-05-16 13:48:20', '0', null, 'MTU1Nzk4NTcwMA==', '', '0', '0.00', '0', null, null);
INSERT INTO `user` VALUES ('66', '0', '00000000000', 'xxx', '1000', '0', '123', null, '0', 'test1557989532', '2019-05-16 14:52:12', '', null, null, null, null, '0', '0', '2', '0', '2019-05-16 14:52:12', '2019-05-16 14:52:12', '0', null, 'MTU1Nzk4OTUzMg==', null, '0', '0.00', '0', null, null);
INSERT INTO `user` VALUES ('68', '0', '00000000000', 'qwe', '1000', '0', '123', null, '0', 'test1557990097', '2019-05-16 15:01:38', '', null, null, null, null, '0', '0', '2', '0', '2019-05-16 15:01:38', '2019-05-16 15:01:38', '0', null, 'MTU1Nzk5MDA5OA==', null, '0', '0.00', '0', null, null);
INSERT INTO `user` VALUES ('69', '0', '00000000000', 'c', '1000', '0', '213', null, '0', 'test1557990246', '2019-05-16 15:04:14', '', null, null, null, null, '0', '0', '2', '0', '2019-05-16 15:04:14', '2019-05-16 15:04:14', '0', null, 'MTU1Nzk5MDI1NA==', null, '0', '0.00', '0', null, null);
INSERT INTO `user` VALUES ('70', '0', '00000000000', 'x', '1000', '0', '123', null, '0', 'test1557990307', '2019-05-16 15:05:06', '', null, null, null, null, '0', '0', '2', '0', '2019-05-16 15:05:06', '2019-05-16 15:05:06', '0', null, 'MTU1Nzk5MDMwNg==', null, '0', '0.00', '0', null, null);
INSERT INTO `user` VALUES ('71', '0', '00000000000', 'z', '1000', '0', '123', null, '0', 'test1557990329', '2019-05-16 15:05:29', '', null, null, null, null, '0', '0', '2', '0', '2019-05-16 15:05:29', '2019-05-16 15:05:29', '0', null, 'MTU1Nzk5MDMyOQ==', null, '0', '0.00', '0', null, null);
INSERT INTO `user` VALUES ('72', '0', '00000000000', 'g', '1000', '0', '123', null, '0', 'test1557990417', '2019-05-16 15:06:57', '', null, null, null, null, '0', '0', '2', '0', '2019-05-16 15:06:57', '2019-05-16 15:06:57', '0', null, 'MTU1OTMwNDEwMw==', null, '0', '0.00', '0', null, null);
INSERT INTO `user` VALUES ('73', '0', '00000000000', 'y', '1000', '0', '213', null, '0', 'test1557990702', '2019-05-16 15:11:42', '', null, null, null, null, '0', '0', '2', '0', '2019-05-16 15:11:42', '2019-05-16 15:11:42', '0', null, 'MTU1Nzk5MDcwMg==', null, '0', '0.00', '0', null, null);
INSERT INTO `user` VALUES ('74', '0', '00000000000', 'h', '1000', '0', '12312', null, '0', 'test1557991211', '2019-05-16 15:20:11', '', null, null, null, null, '0', '0', '2', '0', '2019-05-16 15:20:11', '2019-05-16 15:20:11', '0', null, 'MTU1Nzk5MTIxMQ==', null, '0', '0.00', '0', null, null);
INSERT INTO `user` VALUES ('75', '0', '00000000000', 'd', '1000', '0', '123', null, '0', 'test1557992495', '2019-05-16 15:41:35', '', null, null, null, null, '0', '0', '2', '0', '2019-05-16 15:41:35', '2019-05-16 15:41:35', '0', null, 'MTU1Nzk5MjQ5NQ==', null, '0', '0.00', '0', null, null);
INSERT INTO `user` VALUES ('76', '0', '00000000000', '123d', '1000', '0', '123', null, '0', 'test1557992936', '2019-05-16 15:48:55', '', null, null, null, null, '0', '0', '2', '0', '2019-05-16 15:48:55', '2019-05-16 15:48:55', '0', null, 'MTU1Nzk5MjkzNQ==', null, '0', '0.00', '0', null, null);
INSERT INTO `user` VALUES ('77', '0', '00000000000', 'sdf', '1000', '0', '213', null, '0', 'test1557993037', '2019-05-16 15:50:37', '', null, null, null, null, '0', '0', '2', '0', '2019-05-16 15:50:37', '2019-05-16 15:50:37', '0', null, 'MTU1Nzk5MzAzNw==', null, '0', '0.00', '0', null, null);
INSERT INTO `user` VALUES ('78', '0', '00000000000', 'dfq1', '1000', '0', '123', null, '0', 'test1557993143', '2019-05-16 15:52:22', '', null, null, null, null, '0', '0', '2', '0', '2019-05-16 15:52:22', '2019-05-16 15:52:22', '0', null, 'MTU1Nzk5MzE0Mg==', null, '0', '0.00', '0', null, null);
INSERT INTO `user` VALUES ('79', '0', '00000000000', 'dfss1', '1000', '0', '123', null, '0', 'test1557993200', '2019-05-16 15:53:20', '', null, null, null, null, '0', '0', '2', '0', '2019-05-16 15:53:20', '2019-05-16 15:53:20', '0', null, 'MTU1Nzk5MzIwMA==', null, '0', '0.00', '0', null, null);
INSERT INTO `user` VALUES ('80', '0', '00000000000', 'ewtr', '1000', '0', '123', null, '0', 'test1557993242', '2019-05-16 15:54:01', '', null, null, null, null, '0', '0', '2', '0', '2019-05-16 15:54:01', '2019-05-16 15:54:01', '0', null, 'MTU1Nzk5MzI0MQ==', null, '0', '0.00', '0', null, null);
INSERT INTO `user` VALUES ('81', '0', '00000000000', 'a', '1000', '0', '123', null, '0', 'test1557994121', '2019-05-16 16:08:41', '', null, null, null, null, '0', '0', '2', '0', '2019-05-16 16:08:41', '2019-05-16 16:08:41', '0', null, 'MTU1Nzk5NDEyOA==', null, '0', '0.00', '0', null, null);
INSERT INTO `user` VALUES ('82', '0', '00000000000', '09ab-86cb-b82f', '1000', '0', '', null, '0', '09ab-86cb-b82f', '2019-05-17 09:59:27', '', null, null, null, null, '0', '0', '2', '0', '2019-05-17 09:59:27', '2019-05-17 09:59:27', '1', null, null, null, '0', '0.00', '0', null, '09ab-86cb-b82f');
INSERT INTO `user` VALUES ('83', '0', '00000000000', '3a7f-b51d-ac6e', '1000', '0', '', null, '0', '3a7f-b51d-ac6e', '2019-05-17 10:11:25', '', null, null, null, null, '0', '0', '2', '0', '2019-05-17 10:11:25', '2019-05-17 10:11:25', '1', null, null, null, '0', '0.00', '0', null, '3a7f-b51d-ac6e');
INSERT INTO `user` VALUES ('84', '0', '00000000000', '53a1-9d0a-afba', '1000', '0', '', null, '0', '53a1-9d0a-afba', '2019-05-17 10:12:23', '', null, null, null, null, '0', '0', '2', '0', '2019-05-17 10:12:23', '2019-05-17 10:12:23', '1', null, null, null, '0', '0.00', '0', null, '53a1-9d0a-afba');
INSERT INTO `user` VALUES ('85', '0', '00000000000', 'ee5f-858c-b2eb', '1000', '0', '', null, '0', 'ee5f-858c-b2eb', '2019-05-17 13:42:12', '', null, null, null, null, '0', '0', '2', '0', '2019-05-17 13:42:12', '2019-05-17 13:42:12', '1', null, null, null, '0', '0.00', '0', null, 'ee5f-858c-b2eb');
INSERT INTO `user` VALUES ('86', '0', '00000000000', 'f9ed-8fbb-a4ec', '1000', '0', '', null, '0', 'f9ed-8fbb-a4ec', '2019-05-17 15:56:10', '', null, null, null, null, '0', '0', '2', '0', '2019-05-17 15:56:10', '2019-05-17 15:56:10', '1', null, null, null, '0', '0.00', '0', null, 'f9ed-8fbb-a4ec');
INSERT INTO `user` VALUES ('87', '0', '00000000000', '5bef-a8d1-a55a', '1000', '0', '', null, '0', '5bef-a8d1-a55a', '2019-05-20 09:57:05', '', null, null, null, null, '0', '0', '2', '0', '2019-05-20 09:57:05', '2019-05-20 09:57:05', '1', null, null, null, '0', '0.00', '0', null, '5bef-a8d1-a55a');
INSERT INTO `user` VALUES ('88', '0', '00000000000', '3a7b-b766-ac96', '1000', '0', '', null, '0', '3a7b-b766-ac96', '2019-05-20 09:57:08', '', null, null, null, null, '0', '0', '2', '0', '2019-05-20 09:57:08', '2019-05-20 09:57:08', '1', null, null, null, '0', '0.00', '0', null, '3a7b-b766-ac96');
INSERT INTO `user` VALUES ('89', '0', '00000000000', 'd2b9-b83a-bf6d', '1000', '0', '', null, '0', 'd2b9-b83a-bf6d', '2019-05-20 09:57:09', '', null, null, null, null, '0', '0', '2', '0', '2019-05-20 09:57:09', '2019-05-20 09:57:09', '1', null, null, null, '0', '0.00', '0', null, 'd2b9-b83a-bf6d');
INSERT INTO `user` VALUES ('90', '0', '00000000000', '18380582455', '1000', '0', '123456', null, '0', 'test1558317698', '2019-05-20 10:01:36', '', null, null, null, null, '0', '0', '2', '0', '2019-05-20 10:01:36', '2019-05-20 10:01:36', '0', null, 'MTU1OTMwNDk5Mw==', null, '0', '0.00', '0', null, null);
INSERT INTO `user` VALUES ('91', '0', '00000000000', '0c3e-94bf-bbb6', '1000', '0', '', null, '0', '0c3e-94bf-bbb6', '2019-05-20 10:01:51', '', null, null, null, null, '0', '0', '2', '0', '2019-05-20 10:01:51', '2019-05-20 10:01:51', '1', null, null, null, '0', '0.00', '0', null, '0c3e-94bf-bbb6');
INSERT INTO `user` VALUES ('92', '0', '00000000000', '0125-b07c-ae4b', '1000', '0', '', null, '0', '0125-b07c-ae4b', '2019-05-20 10:01:53', '', null, null, null, null, '0', '0', '2', '0', '2019-05-20 10:01:53', '2019-05-20 10:01:53', '1', null, null, null, '0', '0.00', '0', null, '0125-b07c-ae4b');
INSERT INTO `user` VALUES ('93', '0', '00000000000', 'fc9a-8acb-8fc9', '1000', '0', '', null, '0', 'fc9a-8acb-8fc9', '2019-05-20 10:49:08', '', null, null, null, null, '0', '0', '2', '0', '2019-05-20 10:49:08', '2019-05-20 10:49:08', '1', null, null, null, '0', '0.00', '0', null, 'fc9a-8acb-8fc9');
INSERT INTO `user` VALUES ('94', '0', '00000000000', '18380582456', '1000', '0', '123456', null, '0', 'test1558320593', '2019-05-20 10:49:51', '', null, null, null, null, '0', '0', '2', '0', '2019-05-20 10:49:51', '2019-05-20 10:49:51', '0', null, 'MTU1OTIwNzYyNA==', null, '0', '0.00', '0', null, null);
INSERT INTO `user` VALUES ('95', '0', '00000000000', 'dc3a-829a-8523', '1000', '0', '', null, '0', 'dc3a-829a-8523', '2019-05-20 10:50:57', '', null, null, null, null, '0', '0', '2', '0', '2019-05-20 10:50:57', '2019-05-20 10:50:57', '1', null, null, null, '0', '0.00', '0', null, 'dc3a-829a-8523');
INSERT INTO `user` VALUES ('96', '0', '00000000000', '594f-97d3-bf15', '1000', '0', '', null, '0', '594f-97d3-bf15', '2019-05-20 14:16:37', '', null, null, null, null, '0', '0', '2', '0', '2019-05-20 14:16:37', '2019-05-20 14:16:37', '1', null, null, null, '0', '0.00', '0', null, '594f-97d3-bf15');
INSERT INTO `user` VALUES ('97', '0', '00000000000', '3c39-bfd1-8fc6', '1000', '0', '', null, '0', '3c39-bfd1-8fc6', '2019-05-20 14:16:46', '', null, null, null, null, '0', '0', '2', '0', '2019-05-20 14:16:46', '2019-05-20 14:16:46', '1', null, null, null, '0', '0.00', '0', null, '3c39-bfd1-8fc6');
INSERT INTO `user` VALUES ('98', '0', '00000000000', 'b16a-a6a9-846e', '1000', '0', '', null, '0', 'b16a-a6a9-846e', '2019-05-20 14:17:23', '', null, null, null, null, '0', '0', '2', '0', '2019-05-20 14:17:23', '2019-05-20 14:17:23', '1', null, null, null, '0', '0.00', '0', null, 'b16a-a6a9-846e');
INSERT INTO `user` VALUES ('99', '0', '00000000000', '60f1-9781-aac4', '1000', '0', '', null, '0', '60f1-9781-aac4', '2019-05-20 14:20:41', '', null, null, null, null, '0', '0', '2', '0', '2019-05-20 14:20:41', '2019-05-20 14:20:41', '1', null, null, null, '0', '0.00', '0', null, '60f1-9781-aac4');
INSERT INTO `user` VALUES ('100', '0', '00000000000', 'acd8-bec2-907f', '1000', '0', '', null, '0', 'acd8-bec2-907f', '2019-05-20 14:21:24', '', null, null, null, null, '0', '0', '2', '0', '2019-05-20 14:21:24', '2019-05-20 14:21:24', '1', null, null, null, '0', '0.00', '0', null, 'acd8-bec2-907f');
INSERT INTO `user` VALUES ('101', '0', '00000000000', 'f8c3-a92b-a212', '1000', '0', '', null, '0', 'f8c3-a92b-a212', '2019-05-20 14:24:12', '', null, null, null, null, '0', '0', '2', '0', '2019-05-20 14:24:12', '2019-05-20 14:24:12', '1', null, null, null, '0', '0.00', '0', null, 'f8c3-a92b-a212');
INSERT INTO `user` VALUES ('102', '0', '00000000000', '25c7-9373-ac23', '1000', '0', '', null, '0', '25c7-9373-ac23', '2019-05-20 14:24:13', '', null, null, null, null, '0', '0', '2', '0', '2019-05-20 14:24:13', '2019-05-20 14:24:13', '1', null, null, null, '0', '0.00', '0', null, '25c7-9373-ac23');
INSERT INTO `user` VALUES ('103', '0', '00000000000', '3807-aa66-b5c8', '1000', '0', '', null, '0', '3807-aa66-b5c8', '2019-05-20 16:34:55', '', null, null, null, null, '0', '0', '2', '0', '2019-05-20 16:34:55', '2019-05-20 16:34:55', '1', null, null, null, '0', '0.00', '0', null, '3807-aa66-b5c8');
INSERT INTO `user` VALUES ('104', '0', '00000000000', 'ee8b-9f8f-8b10', '1000', '0', '', null, '0', 'ee8b-9f8f-8b10', '2019-05-22 11:42:04', '', null, null, null, null, '0', '0', '2', '0', '2019-05-22 11:42:04', '2019-05-22 11:42:04', '1', null, null, null, '0', '0.00', '0', null, 'ee8b-9f8f-8b10');
INSERT INTO `user` VALUES ('105', '0', '00000000000', '1658-bdb0-a814', '1000', '0', '', null, '0', '1658-bdb0-a814', '2019-05-22 13:54:58', '', null, null, null, null, '0', '0', '2', '0', '2019-05-22 13:54:58', '2019-05-22 13:54:58', '1', null, null, null, '0', '0.00', '0', null, '1658-bdb0-a814');
INSERT INTO `user` VALUES ('106', '0', '00000000000', 'test9', '1000', '0', '123456', null, '0', 'test', '2019-05-25 06:23:24', '', null, null, null, null, '0', '0', '2', '0', '2019-05-25 06:23:24', '2019-05-25 06:23:24', '1', null, 'MTU2Nzc2MDUyNw==', null, '0', '0.00', '0', null, null);
INSERT INTO `user` VALUES ('108', '0', '00000000000', 'test10', '1000', '0', '123456', null, '0', 'test', '2019-05-25 06:27:34', '', null, null, null, null, '0', '0', '2', '0', '2019-05-25 06:27:34', '2019-05-25 06:27:34', '1', null, 'MTU2Nzc2MDUyNw==', null, '0', '0.00', '0', null, null);
INSERT INTO `user` VALUES ('109', '0', '00000000000', 'test102', '1000', '0', '123456', null, '0', 'test', '2019-05-25 06:31:20', '', null, null, null, null, '0', '0', '2', '0', '2019-05-25 06:31:20', '2019-05-25 06:31:20', '1', null, 'MTU1ODczNzA4MA==', null, '0', '0.00', '0', null, null);
INSERT INTO `user` VALUES ('110', '0', '00000000000', 'da3a-aec8-9155', '1000', '0', '', null, '0', 'da3a-aec8-9155', '2019-05-28 10:02:50', '', null, null, null, null, '0', '0', '2', '0', '2019-05-28 10:02:50', '2019-05-28 10:02:50', '1', null, null, null, '0', '0.00', '0', null, 'da3a-aec8-9155');
INSERT INTO `user` VALUES ('111', '0', '00000000000', '72c3-892b-811d', '1000', '0', '', null, '0', '72c3-892b-811d', '2019-05-28 10:31:27', '', null, null, null, null, '0', '0', '2', '0', '2019-05-28 10:31:27', '2019-05-28 10:31:27', '1', null, null, null, '0', '0.00', '0', null, '72c3-892b-811d');
INSERT INTO `user` VALUES ('112', '0', '00000000000', 't', '1000', '0', '123', null, '0', 'test1559040128', '2019-05-28 18:42:07', '', null, null, null, null, '0', '0', '2', '0', '2019-05-28 18:42:07', '2019-05-28 18:42:07', '0', null, 'MTU1OTMwMjM0Nw==', null, '0', '0.00', '0', null, null);
INSERT INTO `user` VALUES ('113', '0', '00000000000', '7fc0-9306-a40e', '1000', '0', '', null, '0', '7fc0-9306-a40e', '2019-05-30 13:46:16', '', null, null, null, null, '0', '0', '2', '0', '2019-05-30 13:46:16', '2019-05-30 13:46:16', '1', null, null, null, '0', '0.00', '0', null, '7fc0-9306-a40e');
INSERT INTO `user` VALUES ('114', '0', '00000000000', 'ca1b-acd7-a7b1', '1000', '0', '', null, '0', 'ca1b-acd7-a7b1', '2019-05-30 13:48:35', '', null, null, null, null, '0', '0', '2', '0', '2019-05-30 13:48:35', '2019-05-30 13:48:35', '1', null, null, null, '0', '0.00', '0', null, 'ca1b-acd7-a7b1');
INSERT INTO `user` VALUES ('115', '0', '00000000000', 'b2ff-8101-b1f4', '1000', '0', '', null, '0', 'b2ff-8101-b1f4', '2019-05-30 13:55:16', '', null, null, null, null, '0', '0', '2', '0', '2019-05-30 13:55:16', '2019-05-30 13:55:16', '1', null, null, null, '0', '0.00', '0', null, 'b2ff-8101-b1f4');
INSERT INTO `user` VALUES ('116', '0', '00000000000', 'b9e0-9b16-8bc4', '1000', '0', '', null, '0', 'b9e0-9b16-8bc4', '2019-05-30 14:00:19', '', null, null, null, null, '0', '0', '2', '0', '2019-05-30 14:00:19', '2019-05-30 14:00:19', '1', null, null, null, '0', '0.00', '0', null, 'b9e0-9b16-8bc4');
INSERT INTO `user` VALUES ('117', '0', '00000000000', '15827450091', '1000', '0', '123456', null, '0', 'test1559196347', '2019-05-30 14:05:47', '', null, null, null, null, '0', '0', '2', '0', '2019-05-30 14:05:47', '2019-05-30 14:05:47', '0', null, 'MTU1OTI5NjY1MA==', null, '0', '0.00', '0', null, null);
INSERT INTO `user` VALUES ('118', '0', '00000000000', '3e07-852f-931f', '1000', '0', '', null, '0', '3e07-852f-931f', '2019-05-30 15:19:54', '', null, null, null, null, '0', '0', '2', '0', '2019-05-30 15:19:54', '2019-05-30 15:19:54', '1', null, null, null, '0', '0.00', '0', null, '3e07-852f-931f');
INSERT INTO `user` VALUES ('119', '0', '00000000000', 'e73b-a185-8155', '1000', '0', '', null, '0', 'e73b-a185-8155', '2019-05-31 15:58:22', '', null, null, null, null, '0', '0', '2', '0', '2019-05-31 15:58:22', '2019-05-31 15:58:22', '1', null, null, null, '0', '0.00', '0', null, 'e73b-a185-8155');
INSERT INTO `user` VALUES ('120', '0', '00000000000', 'b783-afd3-b9cd', '1000', '0', '', null, '0', 'b783-afd3-b9cd', '2019-05-31 16:04:02', '', null, null, null, null, '0', '0', '2', '0', '2019-05-31 16:04:02', '2019-05-31 16:04:02', '1', null, null, null, '0', '0.00', '0', null, 'b783-afd3-b9cd');

-- ----------------------------
-- Event structure for test
-- ----------------------------
DROP EVENT IF EXISTS `test`;
DELIMITER ;;
CREATE DEFINER=`root`@`%` EVENT `test` ON SCHEDULE EVERY '0:15' MINUTE_SECOND STARTS '2019-05-18 14:34:33' ON COMPLETION NOT PRESERVE ENABLE DO update op_mail_sends set status = 0 where send_id = 37
;;
DELIMITER ;
SET FOREIGN_KEY_CHECKS=1;
